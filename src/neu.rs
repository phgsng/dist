mod sum
{
    #[inline(always)]
    pub fn scalar(a: f64, b: f64) -> (f64, f64)
    {
        let t = a + b;
        let p = if a.abs() >= b.abs() { (a - t) + b } else { (b - t) + a };

        (t, p)
    }

    #[inline(always)]
    pub fn slices(x: &[f64], y: &[f64], s: f64, c: f64) -> (f64, f64)
    {
        x.iter().zip(y.iter()).map(|(x, y)| x * y).fold(
            (s, c),
            |(s, c), summand| {
                let (s, cc) = scalar(s, summand);
                (s, c + cc)
            },
        )
    }

    #[inline(always)]
    pub(crate) fn summed(x: &[f64], y: &[f64], s: f64, c: f64) -> f64
    {
        let (s, c) = slices(x, y, s, c);

        s + c
    }
}

mod generic
{
    use super::*;

    #[inline(always)]
    pub fn dot_chunk<const LANES: usize>(
        x: &[f64; LANES],
        y: &[f64; LANES],
        s: &mut [f64; LANES],
        c: &mut [f64; LANES],
    )
    {
        let products: [f64; LANES] = array_init::array_init(|n| x[n] * y[n]);
        let t: [(f64, f64); LANES] = array_init::array_init(|n| {
            if s[n].abs() >= products[n].abs() {
                (s[n], products[n])
            } else {
                (products[n], s[n])
            }
        });

        s.iter_mut().zip(products.into_iter()).for_each(|(s, a)| *s += a);
        c.iter_mut()
            .zip(t.into_iter())
            .zip(s.iter())
            .for_each(|((c, (t0, t1)), s)| *c += (t0 - s) + t1);
    }

    pub fn dot<const LANES: usize>(x: &[f64], y: &[f64]) -> f64
    {
        assert_eq!(x.len(), y.len());

        let mut x = x.chunks_exact(LANES);
        let mut y = y.chunks_exact(LANES);

        let mut s = [0.0; LANES];
        let mut c = [0.0; LANES];

        loop {
            let Some(x) = x.next() else { break };
            let Some(y) = y.next() else { break };

            let x = x.try_into().unwrap();
            let y = y.try_into().unwrap();

            dot_chunk_generic::<LANES>(x, y, &mut s, &mut c);
        }

        let c = c.into_iter().sum();
        let (s, c) = s.into_iter().fold((0.0, c), |(s, c), summand| {
            let (s, cc) = sum::scalar(s, summand);
            (s, c + cc)
        });

        let x = x.remainder();
        let y = y.remainder();

        sum::summed(x, y, c, s)
    }
}

pub use generic::{dot as dot_generic, dot_chunk as dot_chunk_generic};

pub mod simd
{
    use super::sum;

    use packed_simd::{f64x4, f64x8, m64x4, m64x8};

    macro_rules! impl_dot_chunk_simd {
        ($fn:ident, $t:ty, $m:ty) => {
            #[inline(always)]
            pub fn $fn(x: $t, y: $t, s: &mut $t, c: &mut $t)
            {
                let product: $t = x * y;

                let mask: $m = s.abs().ge(product.abs());

                let t0: $t = mask.select(*s, product);
                let t1: $t = mask.select(product, *s);

                *s += product;
                *c += (t0 - *s) + t1;
            }
        };
    }

    impl_dot_chunk_simd!(dot_chunk_simd_x4, f64x4, m64x4);
    impl_dot_chunk_simd!(dot_chunk_simd_x8, f64x8, m64x8);

    macro_rules! impl_dot_simd {
        ($fn:ident, $cfn:ident, $t:ty, $m:ty) => {
            #[multiversion::multiversion(targets = "simd")]
            pub fn $fn(x: &[f64], y: &[f64]) -> f64
            {
                assert_eq!(x.len(), y.len());

                let mut s = <$t>::default();
                let mut c = <$t>::default();

                let mut x = x.chunks_exact(<$t>::lanes());
                let mut y = y.chunks_exact(<$t>::lanes());

                #[inline(always)]
                fn cast(vs: &[f64]) -> $t
                {
                    let vs: [f64; <$t>::lanes()] = vs.try_into().unwrap();
                    <$t>::from(vs)
                }

                loop {
                    let Some(x) = x.next() else { break };
                    let Some(y) = y.next() else { break };

                    let x = cast(x);
                    let y = cast(y);

                    $cfn(x, y, &mut s, &mut c);
                }

                let c: f64 = c.sum();
                let s: [f64; <$t>::lanes()] = s.into();

                let (s, c) = s.into_iter().fold((0.0, c), |(s, c), summand| {
                    let (s, cc) = sum::scalar(s, summand);
                    (s, c + cc)
                });

                let x = x.remainder();
                let y = y.remainder();

                sum::summed(x, y, c, s)
            }
        };
    }

    impl_dot_simd!(dot_simd_x4, dot_chunk_simd_x4, f64x4, m64x4);
    impl_dot_simd!(dot_simd_x8, dot_chunk_simd_x8, f64x8, m64x8);
}

pub use simd::{dot_chunk_simd_x4, dot_chunk_simd_x8, dot_simd_x4, dot_simd_x8};

#[cfg(test)]
mod dot_chunk
{
    use super::*;

    #[test]
    fn zero4()
    {
        let z8_0 = [0.0; 4];
        let z8_1 = [0.0; 4];
        let mut sum8 = [0.0; 4];
        let mut c8 = [0.0; 4];

        dot_chunk_generic(&z8_0, &z8_1, &mut sum8, &mut c8);
        assert_eq!(sum8, [0.0; 4]);
        assert_eq!(c8, [0.0; 4]);
    }

    #[test]
    fn zero8()
    {
        let z8_0 = [0.0; 8];
        let z8_1 = [0.0; 8];
        let mut sum8 = [0.0; 8];
        let mut c8 = [0.0; 8];

        dot_chunk_generic(&z8_0, &z8_1, &mut sum8, &mut c8);
        assert_eq!(sum8, [0.0; 8]);
        assert_eq!(c8, [0.0; 8]);
    }

    #[test]
    fn zero4_simd()
    {
        use packed_simd::f64x4;
        let z8_0 = f64x4::default();
        let z8_1 = f64x4::default();
        let mut sum8 = f64x4::default();
        let mut c8 = f64x4::default();

        dot_chunk_simd_x4(z8_0, z8_1, &mut sum8, &mut c8);
        assert_eq!(sum8, f64x4::default());
        assert_eq!(c8, f64x4::default());
    }

    #[test]
    fn zero8_simd()
    {
        use packed_simd::f64x8;
        let z8_0 = f64x8::default();
        let z8_1 = f64x8::default();
        let mut sum8 = f64x8::default();
        let mut c8 = f64x8::default();

        dot_chunk_simd_x8(z8_0, z8_1, &mut sum8, &mut c8);
        assert_eq!(sum8, f64x8::default());
        assert_eq!(c8, f64x8::default());
    }

    #[test]
    fn one4()
    {
        let z8_0 = [1.0; 4];
        let z8_1 = [1.0; 4];
        let mut sum8 = [0.0; 4];
        let mut c8 = [0.0; 4];

        dot_chunk_generic(&z8_0, &z8_1, &mut sum8, &mut c8);
        assert_eq!(sum8, [1.0; 4]);
        assert_eq!(c8, [0.0; 4]);
    }

    #[test]
    fn one8()
    {
        let z8_0 = [1.0; 8];
        let z8_1 = [1.0; 8];
        let mut sum8 = [0.0; 8];
        let mut c8 = [0.0; 8];

        dot_chunk_generic(&z8_0, &z8_1, &mut sum8, &mut c8);
        assert_eq!(sum8, [1.0; 8]);
        assert_eq!(c8, [0.0; 8]);
    }

    #[test]
    fn one1337()
    {
        let z8_0 = [1.0; 1337];
        let z8_1 = [1.0; 1337];
        let mut sum8 = [0.0; 1337];
        let mut c8 = [0.0; 1337];

        dot_chunk_generic(&z8_0, &z8_1, &mut sum8, &mut c8);
        assert_eq!(sum8, [1.0; 1337]);
        assert_eq!(c8, [0.0; 1337]);
    }

    #[test]
    fn one4_simd()
    {
        use packed_simd::f64x4;

        let z8_0 = f64x4::splat(1.0);
        let z8_1 = f64x4::splat(1.0);
        let mut sum8 = f64x4::default();
        let mut c8 = f64x4::default();

        dot_chunk_simd_x4(z8_0, z8_1, &mut sum8, &mut c8);
        assert_eq!(sum8, f64x4::splat(1.0));
        assert_eq!(c8, f64x4::default());
    }

    #[test]
    fn one8_simd()
    {
        use packed_simd::f64x8;

        let z8_0 = f64x8::splat(1.0);
        let z8_1 = f64x8::splat(1.0);
        let mut sum8 = f64x8::default();
        let mut c8 = f64x8::default();

        dot_chunk_simd_x8(z8_0, z8_1, &mut sum8, &mut c8);
        assert_eq!(sum8, f64x8::splat(1.0));
        assert_eq!(c8, f64x8::default());
    }
}

#[cfg(test)]
mod dot
{
    use super::*;

    #[test]
    fn short()
    {
        let a = vec![1.0, 2.0, 3.0];
        let b = vec![1.0, 1.0, 1.0];

        assert_eq!(dot_generic::<4>(&a, &b), 6.0);
        assert_eq!(dot_generic::<8>(&a, &b), 6.0);
        assert_eq!(dot_simd_x4(&a, &b), 6.0);
        assert_eq!(dot_simd_x8(&a, &b), 6.0);
    }

    #[test]
    fn chunks()
    {
        let a = vec![1.0, 2.0, 3.0, 4.0, 1.0, 2.0, 3.0, 4.0];
        let b = vec![1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0];

        assert_eq!(dot_generic::<4>(&a, &b), 20.0);
        assert_eq!(dot_generic::<8>(&a, &b), 20.0);
        assert_eq!(dot_simd_x4(&a, &b), 20.0);
        assert_eq!(dot_simd_x8(&a, &b), 20.0);
    }

    #[test]
    fn chunks_more()
    {
        let a = vec![1.0, 2.0, 3.0, 4.0, 1.0, 2.0, 3.0, 4.0, 1.0];
        let b = vec![1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0];

        assert_eq!(dot_generic::<4>(&a, &b), 21.0);
        assert_eq!(dot_generic::<8>(&a, &b), 21.0);
        assert_eq!(dot_simd_x4(&a, &b), 21.0);
        assert_eq!(dot_simd_x8(&a, &b), 21.0);
    }
}
