use multiversion::multiversion;

pub mod neu;
pub mod tiled;

/**
 * Ensure that a) we handle at least a lane of work per thread and
 * b) we don’t have to care for the remainder in the per-thread loop.
 */
#[inline(always)]
const fn chunk_size(n: usize, lanes: usize, threads: usize) -> usize
{
    let cs = n / threads;
    if cs < lanes {
        return lanes;
    }
    let cs = cs - (cs % lanes);
    if cs < lanes {
        return lanes;
    }
    cs
}

#[inline(never)]
pub fn dist_naive(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    dist_naive_inner(x0, x1, diversities)
}

#[inline(never)]
//#[multiversion(targets("x86_64+avx", "x86+sse"))]
#[multiversion(targets = "simd")]
pub fn dist_naive_dyn(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    dist_naive_inner(x0, x1, diversities)
}

#[inline(always)]
fn dist_naive_inner(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    assert_eq!(x0.len(), x1.len());
    assert_eq!(x0.len(), diversities.len());

    x0.iter()
        .zip(x1.iter())
        .zip(diversities.iter())
        .map(|((x0, x1), d)| {
            let diff = x0 - x1;
            diff * diff * d * d
        })
        .sum::<f64>()
        .sqrt()
}

#[inline(never)]
pub fn dist_naive_par(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    dist_naive_par_inner(x0, x1, diversities)
}

#[inline(never)]
#[multiversion(targets = "simd")]
pub fn dist_naive_par_dyn(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    dist_naive_par_inner(x0, x1, diversities)
}

#[inline(always)]
fn dist_naive_par_inner(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    use rayon::prelude::*;

    assert_eq!(x0.len(), x1.len());
    assert_eq!(x0.len(), diversities.len());

    let chunk_size = chunk_size(x0.len(), 2, rayon::max_num_threads());

    //eprintln!(
    //    "\n\nnaive: working on size={} chunks (n={}, threads={}, remainder={})",
    //    chunk_size,
    //    x1.len(),
    //    rayon::max_num_threads(),
    //    x1.len() % chunk_size,
    //);

    #[inline(always)]
    fn per_par_chunk(x0: &[f64], x1: &[f64], d: &[f64]) -> f64
    {
        let sum = x0
            .iter()
            .zip(x1.iter())
            .zip(d.iter())
            .map(|((x0, x1), d)| {
                let diff = x0 - x1;
                diff * diff * d * d
            })
            .sum::<f64>();

        //eprintln!("naive: chunk sum: {}", sum);

        sum
    }

    let sum = x0
        .par_chunks_exact(chunk_size)
        .zip(x1.par_chunks_exact(chunk_size))
        .zip(diversities.par_chunks_exact(chunk_size))
        .map(|((x0, x1), d)| per_par_chunk(x0, x1, d))
        .sum::<f64>();

    //eprintln!("naive: parallel sum: {}", sum);

    /* The remainders are a bit nasty to work with as the ``Zip`` and ``Map``
     * adaptors used above consume the original iterators so we have to recreate
     * them to obtain the remainders. */

    let sum = x0
        .par_chunks_exact(chunk_size)
        .remainder()
        .into_iter()
        .zip(x1.par_chunks_exact(chunk_size).remainder().into_iter())
        .zip(diversities.par_chunks_exact(chunk_size).remainder().into_iter())
        .fold(sum, |acc, ((x0, x1), d)| {
            let diff = x0 - x1;
            acc + diff * diff * d * d
        });

    //eprintln!("naive: total sum: {}", sum);

    sum.sqrt()
}

#[inline(never)]
pub fn dist_wide_x4(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    dist_wide_x4_inner(x0, x1, diversities)
}

#[inline(never)]
//#[multiversion(targets("x86_64+avx", "x86+sse"))]
#[multiversion(targets = "simd")]
pub fn dist_wide_x4_dyn(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    dist_wide_x4_inner(x0, x1, diversities)
}

#[inline(always)]
fn dist_wide_x4_inner(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    use wide::f64x4;

    assert_eq!(x0.len(), x1.len());
    assert_eq!(x0.len(), diversities.len());

    #[inline(always)]
    fn into4(vs: &[f64]) -> f64x4
    {
        let vs: [f64; 4] = vs.try_into().unwrap();
        f64x4::from(vs)
    }

    let mut x0 = x0.chunks_exact(4);
    let mut x1 = x1.chunks_exact(4);
    let mut d = diversities.chunks_exact(4);

    let mut sum4 = f64x4::ZERO;

    loop {
        let Some(x0) = x0.next() else { break };
        let Some(x1) = x1.next() else { break };
        let Some(d) = d.next() else { break };

        let x0 = into4(x0);
        let x1 = into4(x1);
        let d = into4(d);
        let diff = x0 - x1;
        sum4 += diff * diff * d * d;
    }

    let mut sum = sum4.reduce_add();

    let x0 = x0.remainder().into_iter();
    let x1 = x1.remainder().into_iter();
    let d = d.remainder().into_iter();

    sum += x0
        .zip(x1)
        .zip(d)
        .map(|((x0, x1), d)| {
            let diff = x0 - x1;
            diff * diff * d * d
        })
        .sum::<f64>();

    sum.sqrt()
}

#[inline(never)]
pub fn dist_wide_x4_par(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    dist_wide_x4_par_inner(x0, x1, diversities)
}

#[inline(never)]
#[multiversion(targets = "simd")]
pub fn dist_wide_x4_par_dyn(x0: &[f64], x1: &[f64], diversities: &[f64])
    -> f64
{
    dist_wide_x4_par_inner(x0, x1, diversities)
}

#[inline(always)]
fn dist_wide_x4_par_inner(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    use rayon::prelude::*;
    use wide::f64x4;

    const LANES: usize = 4;

    assert_eq!(x0.len(), x1.len());
    assert_eq!(x0.len(), diversities.len());

    let chunk_size = chunk_size(x0.len(), LANES, rayon::max_num_threads());

    #[inline(always)]
    fn into4(vs: &[f64]) -> f64x4
    {
        let vs: [f64; LANES] = vs.try_into().unwrap();
        f64x4::from(vs)
    }

    #[inline(always)]
    fn per_par_chunk(x0: &[f64], x1: &[f64], d: &[f64]) -> f64x4
    {
        debug_assert!(x0.len() % LANES == 0);
        debug_assert!(x1.len() % LANES == 0);
        debug_assert!(d.len() % LANES == 0);

        debug_assert_eq!(0, x0.chunks_exact(LANES).remainder().len());
        debug_assert_eq!(0, x1.chunks_exact(LANES).remainder().len());
        debug_assert_eq!(0, d.chunks_exact(LANES).remainder().len());

        let x0 = x0.chunks_exact(LANES);
        let x1 = x1.chunks_exact(LANES);
        let d = d.chunks_exact(LANES);

        let x0 = x0.map(into4);
        let x1 = x1.map(into4);
        let d = d.map(into4);

        let sum4 = x0
            .zip(x1)
            .zip(d)
            .map(|((x0, x1), d)| {
                let diff = x0 - x1;
                diff * diff * d * d
            })
            .sum::<f64x4>();

        sum4
    }

    let sum = x0
        .par_chunks_exact(chunk_size)
        .zip(x1.par_chunks_exact(chunk_size))
        .zip(diversities.par_chunks_exact(chunk_size))
        .map(|((x0, x1), d)| per_par_chunk(x0, x1, d))
        .sum::<f64x4>()
        .reduce_add();

    let sum = x0
        .par_chunks_exact(chunk_size)
        .remainder()
        .into_iter()
        .zip(x1.par_chunks_exact(chunk_size).remainder().into_iter())
        .zip(diversities.par_chunks_exact(chunk_size).remainder().into_iter())
        .fold(sum, |acc, ((x0, x1), d)| {
            let diff = x0 - x1;
            acc + diff * diff * d * d
        });

    sum.sqrt()
}

#[inline(never)]
pub fn dist_psmd_x4(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    dist_psmd_x4_inner(x0, x1, diversities)
}

#[inline(never)]
#[multiversion(targets = "simd")]
pub fn dist_psmd_x4_dyn(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    dist_psmd_x4_inner(x0, x1, diversities)
}

#[inline(always)]
pub fn dist_psmd_x4_inner(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    use packed_simd::f64x4;

    assert_eq!(x0.len(), x1.len());
    assert_eq!(x0.len(), diversities.len());

    #[inline(always)]
    fn into4(vs: &[f64]) -> f64x4
    {
        let vs: [f64; f64x4::lanes()] = vs.try_into().unwrap();
        f64x4::from(vs)
    }

    let mut x0 = x0.chunks_exact(f64x4::lanes());
    let mut x1 = x1.chunks_exact(f64x4::lanes());
    let mut d = diversities.chunks_exact(f64x4::lanes());

    let mut sum4 = f64x4::splat(0.0);

    loop {
        let Some(x0) = x0.next() else { break };
        let Some(x1) = x1.next() else { break };
        let Some(d) = d.next() else { break };

        let x0 = into4(x0);
        let x1 = into4(x1);
        let d = into4(d);
        let diff = x0 - x1;
        sum4 += diff * diff * d * d;
    }

    let mut sum = sum4.sum();

    let x0 = x0.remainder().into_iter();
    let x1 = x1.remainder().into_iter();
    let d = d.remainder().into_iter();

    sum += x0
        .zip(x1)
        .zip(d)
        .map(|((x0, x1), d)| {
            let diff = x0 - x1;
            diff * diff * d * d
        })
        .sum::<f64>();

    sum.sqrt()
}

#[inline(never)]
pub fn dist_psmd_x4_par(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    dist_psmd_x4_par_inner(x0, x1, diversities)
}

#[inline(never)]
#[multiversion(targets = "simd")]
pub fn dist_psmd_x4_par_dyn(x0: &[f64], x1: &[f64], diversities: &[f64])
    -> f64
{
    dist_psmd_x4_par_inner(x0, x1, diversities)
}

#[inline(always)]
fn dist_psmd_x4_par_inner(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    use packed_simd::f64x4;
    use rayon::prelude::*;

    assert_eq!(x0.len(), x1.len());
    assert_eq!(x0.len(), diversities.len());

    let chunk_size =
        chunk_size(x0.len(), f64x4::lanes(), rayon::max_num_threads());

    //eprintln!(
    //    "\n\nx4: working on size={} chunks (n={}, threads={}, remainder={})",
    //    chunk_size,
    //    x1.len(),
    //    rayon::max_num_threads(),
    //    x1.len() % chunk_size,
    //);

    #[inline(always)]
    fn into4(vs: &[f64]) -> f64x4
    {
        let vs: [f64; f64x4::lanes()] = vs.try_into().unwrap();
        f64x4::from(vs)
    }

    #[inline(always)]
    fn per_par_chunk(x0: &[f64], x1: &[f64], d: &[f64]) -> f64x4
    {
        debug_assert!(x0.len() % f64x4::lanes() == 0);
        debug_assert!(x1.len() % f64x4::lanes() == 0);
        debug_assert!(d.len() % f64x4::lanes() == 0);

        debug_assert_eq!(0, x0.chunks_exact(f64x4::lanes()).remainder().len());
        debug_assert_eq!(0, x1.chunks_exact(f64x4::lanes()).remainder().len());
        debug_assert_eq!(0, d.chunks_exact(f64x4::lanes()).remainder().len());

        let x0 = x0.chunks_exact(f64x4::lanes());
        let x1 = x1.chunks_exact(f64x4::lanes());
        let d = d.chunks_exact(f64x4::lanes());

        let x0 = x0.map(into4);
        let x1 = x1.map(into4);
        let d = d.map(into4);

        let sum4 = x0
            .zip(x1)
            .zip(d)
            .map(|((x0, x1), d)| {
                let diff = x0 - x1;
                diff * diff * d * d
            })
            .sum::<f64x4>();

        //eprintln!("x4: naive: chunk sum: {}", sum4.sum());

        sum4
    }

    let sum = x0
        .par_chunks_exact(chunk_size)
        .zip(x1.par_chunks_exact(chunk_size))
        .zip(diversities.par_chunks_exact(chunk_size))
        .map(|((x0, x1), d)| per_par_chunk(x0, x1, d))
        .sum::<f64x4>()
        .sum();

    //eprintln!("x4: parallel sum: {}", sum);

    let sum = x0
        .par_chunks_exact(chunk_size)
        .remainder()
        .into_iter()
        .zip(x1.par_chunks_exact(chunk_size).remainder().into_iter())
        .zip(diversities.par_chunks_exact(chunk_size).remainder().into_iter())
        .fold(sum, |acc, ((x0, x1), d)| {
            let diff = x0 - x1;
            acc + diff * diff * d * d
        });

    //eprintln!("x4: total sum: {}", sum);

    sum.sqrt()
}

/** Cache line sized chunks. */
#[inline(never)]
pub fn dist_wide_x4_x2(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    use wide::f64x4;

    const LANES: usize = 4;

    assert_eq!(x0.len(), x1.len());
    assert_eq!(x0.len(), diversities.len());

    #[inline(always)]
    fn into4x2(vs: &[f64]) -> (f64x4, f64x4)
    {
        let vs0: [f64; LANES] = vs[..LANES].try_into().unwrap();
        let vs1: [f64; LANES] = vs[LANES..].try_into().unwrap();

        (f64x4::from(vs0), f64x4::from(vs1))
    }

    let mut x0 = x0.chunks_exact(8);
    let mut x1 = x1.chunks_exact(8);
    let mut d = diversities.chunks_exact(8);

    let mut sum4x2 = (f64x4::ZERO, f64x4::ZERO);

    loop {
        let Some(x0) = x0.next() else { break };
        let Some(x1) = x1.next() else { break };
        let Some(d) = d.next() else { break };

        let x0 = into4x2(x0);
        let x1 = into4x2(x1);
        let d = into4x2(d);

        let diff = (x0.0 - x1.0, x0.1 - x1.1);

        sum4x2.0 += diff.0 * diff.0 * d.0 * d.0;
        sum4x2.1 += diff.1 * diff.1 * d.1 * d.1;
    }

    let mut sum = sum4x2.0.reduce_add() + sum4x2.1.reduce_add();

    let x0 = x0.remainder().into_iter();
    let x1 = x1.remainder().into_iter();
    let d = d.remainder().into_iter();

    sum += x0
        .zip(x1)
        .zip(d)
        .map(|((x0, x1), d)| {
            let diff = x0 - x1;
            diff * diff * d * d
        })
        .sum::<f64>();

    sum.sqrt()
}

/**
 * This will have a small overhead due to the extra branches.
 * Smaller *n* should be more likely in practice so we go from
 * low to high.
 */
#[inline(always)]
pub fn dist_progressive(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    if x0.len() < 1 << 6 {
        dist_wide_x4(x0, x1, diversities)
    } else if x0.len() < 1 << 19 {
        dist_x8_dyn(x0, x1, diversities)
    } else {
        dist_x8_par_dyn(x0, x1, diversities)
    }
}

#[inline(never)]
pub fn dist_x8(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    dist_x8_inner(x0, x1, diversities)
}

#[inline(never)]
//#[multiversion(targets("x86_64+avx", "x86+sse"))]
#[multiversion(targets = "simd")]
pub fn dist_x8_dyn(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    dist_x8_inner(x0, x1, diversities)
}

#[inline(always)]
pub fn dist_x8_inner(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    use packed_simd::f64x8;

    assert_eq!(x0.len(), x1.len());
    assert_eq!(x0.len(), diversities.len());

    #[inline(always)]
    fn into8(vs: &[f64]) -> f64x8
    {
        let vs: [f64; f64x8::lanes()] = vs.try_into().unwrap();
        f64x8::from(vs)
    }

    let mut x0 = x0.chunks_exact(f64x8::lanes());
    let mut x1 = x1.chunks_exact(f64x8::lanes());
    let mut d = diversities.chunks_exact(f64x8::lanes());

    let mut sum8 = f64x8::splat(0.0);

    loop {
        let Some(x0) = x0.next() else { break };
        let Some(x1) = x1.next() else { break };
        let Some(d) = d.next() else { break };

        let x0 = into8(x0);
        let x1 = into8(x1);
        let d = into8(d);
        let diff = x0 - x1;
        sum8 += diff * diff * d * d;
    }

    let mut sum = sum8.sum();

    let x0 = x0.remainder().into_iter();
    let x1 = x1.remainder().into_iter();
    let d = d.remainder().into_iter();

    sum += x0
        .zip(x1)
        .zip(d)
        .map(|((x0, x1), d)| {
            let diff = x0 - x1;
            diff * diff * d * d
        })
        .sum::<f64>();

    sum.sqrt()
}

#[inline(never)]
pub fn dist_x8_par(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    dist_x8_par_inner(x0, x1, diversities)
}

#[inline(never)]
#[multiversion(targets = "simd")]
pub fn dist_x8_par_dyn(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    dist_x8_par_inner(x0, x1, diversities)
}

#[inline(always)]
fn dist_x8_par_inner(x0: &[f64], x1: &[f64], diversities: &[f64]) -> f64
{
    use packed_simd::f64x8;
    use rayon::prelude::*;

    assert_eq!(x0.len(), x1.len());
    assert_eq!(x0.len(), diversities.len());

    let chunk_size =
        chunk_size(x0.len(), f64x8::lanes(), rayon::max_num_threads());

    //eprintln!(
    //    "\n\nx8: working on size={} chunks (n={}, threads={}, remainder={})",
    //    chunk_size,
    //    x1.len(),
    //    rayon::max_num_threads(),
    //    x1.len() % chunk_size,
    //);

    #[inline(always)]
    fn into8(vs: &[f64]) -> f64x8
    {
        let vs: [f64; f64x8::lanes()] = vs.try_into().unwrap();
        f64x8::from(vs)
    }

    #[inline(always)]
    fn per_par_chunk(x0: &[f64], x1: &[f64], d: &[f64]) -> f64x8
    {
        debug_assert!(x0.len() % f64x8::lanes() == 0);
        debug_assert!(x1.len() % f64x8::lanes() == 0);
        debug_assert!(d.len() % f64x8::lanes() == 0);

        debug_assert_eq!(0, x0.chunks_exact(f64x8::lanes()).remainder().len());
        debug_assert_eq!(0, x1.chunks_exact(f64x8::lanes()).remainder().len());
        debug_assert_eq!(0, d.chunks_exact(f64x8::lanes()).remainder().len());

        let x0 = x0.chunks_exact(f64x8::lanes());
        let x1 = x1.chunks_exact(f64x8::lanes());
        let d = d.chunks_exact(f64x8::lanes());

        let x0 = x0.map(into8);
        let x1 = x1.map(into8);
        let d = d.map(into8);

        let sum8 = x0
            .zip(x1)
            .zip(d)
            .map(|((x0, x1), d)| {
                let diff = x0 - x1;
                diff * diff * d * d
            })
            .sum::<f64x8>();

        //eprintln!("x8: naive: chunk sum: {}", sum8.sum());

        sum8
    }

    let sum = x0
        .par_chunks_exact(chunk_size)
        .zip(x1.par_chunks_exact(chunk_size))
        .zip(diversities.par_chunks_exact(chunk_size))
        .map(|((x0, x1), d)| per_par_chunk(x0, x1, d))
        .sum::<f64x8>()
        .sum();

    //eprintln!("x8: parallel sum: {}", sum);

    let sum = x0
        .par_chunks_exact(chunk_size)
        .remainder()
        .into_iter()
        .zip(x1.par_chunks_exact(chunk_size).remainder().into_iter())
        .zip(diversities.par_chunks_exact(chunk_size).remainder().into_iter())
        .fold(sum, |acc, ((x0, x1), d)| {
            let diff = x0 - x1;
            acc + diff * diff * d * d
        });

    //eprintln!("x8: total sum: {}", sum);

    sum.sqrt()
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn chunk_size_ok()
    {
        assert_eq!(8, chunk_size(0, 8, 1));
        assert_eq!(8, chunk_size(1, 8, 1));
        assert_eq!(8, chunk_size(7, 8, 1));
        assert_eq!(8, chunk_size(8, 8, 1));
        assert_eq!(8, chunk_size(9, 8, 1));
        assert_eq!(16, chunk_size(16, 8, 1));
        assert_eq!(16, chunk_size(17, 8, 1));
        assert_eq!(16, chunk_size(23, 8, 1));
        assert_eq!(24, chunk_size(24, 8, 1));
        assert_eq!(24, chunk_size(25, 8, 1));
        assert_eq!(24, chunk_size(25, 8, 1));

        assert_eq!(8, chunk_size(0, 8, 2));
        assert_eq!(8, chunk_size(1, 8, 2));
        assert_eq!(8, chunk_size(7, 8, 2));
        assert_eq!(8, chunk_size(8, 8, 2));
        assert_eq!(8, chunk_size(9, 8, 2));
        assert_eq!(8, chunk_size(16, 8, 2));
        assert_eq!(8, chunk_size(17, 8, 2));
        assert_eq!(8, chunk_size(23, 8, 2));
        assert_eq!(8, chunk_size(24, 8, 2));
        assert_eq!(8, chunk_size(25, 8, 2));
        assert_eq!(8, chunk_size(25, 8, 2));
        assert_eq!(8, chunk_size(31, 8, 2));
        assert_eq!(16, chunk_size(32, 8, 2));
        assert_eq!(16, chunk_size(33, 8, 2));

        assert_eq!(8, chunk_size(0, 8, 4));
        assert_eq!(8, chunk_size(1, 8, 4));
        assert_eq!(8, chunk_size(7, 8, 4));
        assert_eq!(8, chunk_size(8, 8, 4));
        assert_eq!(8, chunk_size(9, 8, 4));
        assert_eq!(8, chunk_size(16, 8, 4));
        assert_eq!(8, chunk_size(17, 8, 4));
        assert_eq!(8, chunk_size(23, 8, 4));
        assert_eq!(8, chunk_size(24, 8, 4));
        assert_eq!(8, chunk_size(25, 8, 4));
        assert_eq!(8, chunk_size(25, 8, 4));
        assert_eq!(8, chunk_size(31, 8, 4));
        assert_eq!(8, chunk_size(32, 8, 4));
        assert_eq!(8, chunk_size(33, 8, 4));
        assert_eq!(8, chunk_size(63, 8, 4));
        assert_eq!(16, chunk_size(64, 8, 4));
        assert_eq!(16, chunk_size(65, 8, 4));
        assert_eq!(16, chunk_size(95, 8, 4));
        assert_eq!(24, chunk_size(96, 8, 4));
        assert_eq!(24, chunk_size(127, 8, 4));
        assert_eq!(32, chunk_size(128, 8, 4));
        assert_eq!(32, chunk_size(144, 8, 4));
    }

    type DistFn = fn(&[f64], &[f64], &[f64]) -> f64;

    #[rustfmt::skip]
    #[test]
    fn fixed_inputs_ok()
    {
        fn fixed(name: &str, dist: DistFn) {
            assert_eq!(dist(&[]    , &[]      , &[]    ),     0.0, "failed for {}", name);
            assert_eq!(dist(&[0.0] , &[0.0]   , &[0.0] ),     0.0, "failed for {}", name);
            assert_eq!(dist(&[1.0] , &[1.0]   , &[1.0] ),     0.0, "failed for {}", name);
            assert_eq!(dist(&[2.0] , &[2.0]   , &[2.0] ),     0.0, "failed for {}", name);
            assert_eq!(dist(&[2.0] , &[1.0]   , &[1.0] ),     1.0, "failed for {}", name);
            assert_eq!(dist(&[42.0], &[1337.0], &[23.0]), 29785.0, "failed for {}", name);
        }

        fixed("naive"          , dist_naive);
        fixed("naive+dyn"      , dist_naive_dyn);
        fixed("naive+par"      , dist_naive_par);
        fixed("naive+par+dyn"  , dist_naive_par_dyn);
        fixed("x4-wide"        , dist_wide_x4);
        fixed("x4-wide+dyn"    , dist_wide_x4_dyn);
        fixed("x4-wide+par"    , dist_wide_x4_par);
        fixed("x4-wide+par+dyn", dist_wide_x4_par_dyn);
        fixed("x4-psmd"        , dist_psmd_x4);
        fixed("x4-psmd+dyn"    , dist_psmd_x4_dyn);
        fixed("x4-psmd+par"    , dist_psmd_x4_par);
        fixed("x4-psmd+par+dyn", dist_psmd_x4_par_dyn);
        fixed("x4-widex2"      , dist_wide_x4_x2);
        fixed("x8-wide"        , dist_x8);
        fixed("x8-wide+dyn"    , dist_x8_dyn);
        fixed("x8-wide+par"    , dist_x8_par);
        fixed("x8-wide+par+dyn", dist_x8_par_dyn);
    }

    #[test]
    fn rand_ok()
    {
        fn mk_inputs(n: usize) -> (Vec<f64>, Vec<f64>, Vec<f64>)
        {
            use rand_chacha::{rand_core::{RngCore, SeedableRng},
                              ChaCha8Rng};

            let mut rng = ChaCha8Rng::seed_from_u64(42);

            let mut x = vec![0.0; n];
            let mut y = vec![0.0; n];
            let mut d = vec![0.0; n];

            x.fill_with(|| rng.next_u32() as f64);
            y.fill_with(|| rng.next_u32() as f64);
            d.fill_with(|| rng.next_u32() as f64);

            (x, y, d)
        }

        let (x, y, d) = mk_inputs(1);
        assert_eq!(1.2362616064925038e18, dist_naive(&x, &y, &d));
        assert_eq!(1.2362616064925038e18, dist_naive_dyn(&x, &y, &d));
        assert_eq!(1.2362616064925038e18, dist_naive_par(&x, &y, &d));
        assert_eq!(1.2362616064925038e18, dist_naive_par_dyn(&x, &y, &d));
        assert_eq!(1.2362616064925038e18, dist_wide_x4(&x, &y, &d));
        assert_eq!(1.2362616064925038e18, dist_wide_x4_dyn(&x, &y, &d));
        assert_eq!(1.2362616064925038e18, dist_wide_x4_par(&x, &y, &d));
        assert_eq!(1.2362616064925038e18, dist_wide_x4_par_dyn(&x, &y, &d));
        assert_eq!(1.2362616064925038e18, dist_psmd_x4(&x, &y, &d));
        assert_eq!(1.2362616064925038e18, dist_psmd_x4_dyn(&x, &y, &d));
        assert_eq!(1.2362616064925038e18, dist_psmd_x4_par(&x, &y, &d));
        assert_eq!(1.2362616064925038e18, dist_psmd_x4_par_dyn(&x, &y, &d));
        assert_eq!(1.2362616064925038e18, dist_wide_x4_x2(&x, &y, &d));
        assert_eq!(1.2362616064925038e18, dist_x8(&x, &y, &d));
        assert_eq!(1.2362616064925038e18, dist_x8_dyn(&x, &y, &d));
        assert_eq!(1.2362616064925038e18, dist_x8_par(&x, &y, &d));
        assert_eq!(1.2362616064925038e18, dist_x8_par_dyn(&x, &y, &d));

        let (x, y, d) = mk_inputs(2);
        assert_eq!(2.3884802520190817e18, dist_naive(&x, &y, &d));
        assert_eq!(2.3884802520190817e18, dist_naive_dyn(&x, &y, &d));
        assert_eq!(2.3884802520190817e18, dist_naive_par(&x, &y, &d));
        assert_eq!(2.3884802520190817e18, dist_naive_par_dyn(&x, &y, &d));
        assert_eq!(2.3884802520190817e18, dist_wide_x4(&x, &y, &d));
        assert_eq!(2.3884802520190817e18, dist_wide_x4_dyn(&x, &y, &d));
        assert_eq!(2.3884802520190817e18, dist_wide_x4_par(&x, &y, &d));
        assert_eq!(2.3884802520190817e18, dist_wide_x4_par_dyn(&x, &y, &d));
        assert_eq!(2.3884802520190817e18, dist_psmd_x4(&x, &y, &d));
        assert_eq!(2.3884802520190817e18, dist_psmd_x4_dyn(&x, &y, &d));
        assert_eq!(2.3884802520190817e18, dist_psmd_x4_par(&x, &y, &d));
        assert_eq!(2.3884802520190817e18, dist_psmd_x4_par_dyn(&x, &y, &d));
        assert_eq!(2.3884802520190817e18, dist_wide_x4_x2(&x, &y, &d));
        assert_eq!(2.3884802520190817e18, dist_x8(&x, &y, &d));
        assert_eq!(2.3884802520190817e18, dist_x8_dyn(&x, &y, &d));
        assert_eq!(2.3884802520190817e18, dist_x8_par(&x, &y, &d));
        assert_eq!(2.3884802520190817e18, dist_x8_par_dyn(&x, &y, &d));

        let (x, y, d) = mk_inputs(3);
        assert_eq!(6.079134667570828e18, dist_naive(&x, &y, &d));
        assert_eq!(6.079134667570828e18, dist_naive_dyn(&x, &y, &d));
        assert_eq!(6.079134667570828e18, dist_naive_par(&x, &y, &d));
        assert_eq!(6.079134667570828e18, dist_naive_par_dyn(&x, &y, &d));
        assert_eq!(6.079134667570828e18, dist_wide_x4(&x, &y, &d));
        assert_eq!(6.079134667570828e18, dist_wide_x4_dyn(&x, &y, &d));
        assert_eq!(6.079134667570828e18, dist_wide_x4_par(&x, &y, &d));
        assert_eq!(6.079134667570828e18, dist_wide_x4_par_dyn(&x, &y, &d));
        assert_eq!(6.079134667570828e18, dist_psmd_x4(&x, &y, &d));
        assert_eq!(6.079134667570828e18, dist_psmd_x4_dyn(&x, &y, &d));
        assert_eq!(6.079134667570828e18, dist_psmd_x4_par(&x, &y, &d));
        assert_eq!(6.079134667570828e18, dist_psmd_x4_par_dyn(&x, &y, &d));
        assert_eq!(6.079134667570828e18, dist_wide_x4_x2(&x, &y, &d));
        assert_eq!(6.079134667570828e18, dist_x8(&x, &y, &d));
        assert_eq!(6.079134667570828e18, dist_x8_dyn(&x, &y, &d));
        assert_eq!(6.079134667570828e18, dist_x8_par(&x, &y, &d));
        assert_eq!(6.079134667570828e18, dist_x8_par_dyn(&x, &y, &d));

        let (x, y, d) = mk_inputs(5);
        assert_eq!(4.252616612110364e18, dist_naive(&x, &y, &d));
        assert_eq!(4.252616612110364e18, dist_naive_dyn(&x, &y, &d));
        assert_eq!(4.252616612110364e18, dist_naive_par(&x, &y, &d));
        assert_eq!(4.252616612110364e18, dist_naive_par_dyn(&x, &y, &d));
        assert_eq!(4.252616612110364e18, dist_wide_x4(&x, &y, &d));
        assert_eq!(4.252616612110364e18, dist_wide_x4_dyn(&x, &y, &d));
        assert_eq!(4.252616612110364e18, dist_wide_x4_par(&x, &y, &d));
        assert_eq!(4.252616612110364e18, dist_wide_x4_par_dyn(&x, &y, &d));
        assert_eq!(4.252616612110364e18, dist_psmd_x4(&x, &y, &d));
        assert_eq!(4.252616612110364e18, dist_psmd_x4_dyn(&x, &y, &d));
        assert_eq!(4.252616612110364e18, dist_psmd_x4_par(&x, &y, &d));
        assert_eq!(4.252616612110364e18, dist_psmd_x4_par_dyn(&x, &y, &d));
        assert_eq!(4.252616612110364e18, dist_wide_x4_x2(&x, &y, &d));
        assert_eq!(4.252616612110364e18, dist_x8(&x, &y, &d));
        assert_eq!(4.252616612110364e18, dist_x8_dyn(&x, &y, &d));
        assert_eq!(4.252616612110364e18, dist_x8_par(&x, &y, &d));
        assert_eq!(4.252616612110364e18, dist_x8_par_dyn(&x, &y, &d));

        let (x, y, d) = mk_inputs(23);
        assert_eq!(2.001259521712195e19, dist_naive(&x, &y, &d));
        assert_eq!(2.001259521712195e19, dist_naive_dyn(&x, &y, &d));
        assert_eq!(2.001259521712195e19, dist_naive_par(&x, &y, &d));
        assert_eq!(2.001259521712195e19, dist_naive_par_dyn(&x, &y, &d));
        assert_eq!(2.001259521712195e19, dist_wide_x4(&x, &y, &d));
        assert_eq!(2.001259521712195e19, dist_wide_x4_dyn(&x, &y, &d));
        assert_eq!(2.001259521712195e19, dist_wide_x4_par(&x, &y, &d));
        assert_eq!(2.001259521712195e19, dist_wide_x4_par_dyn(&x, &y, &d));
        assert_eq!(2.001259521712195e19, dist_psmd_x4(&x, &y, &d));
        assert_eq!(2.001259521712195e19, dist_psmd_x4_dyn(&x, &y, &d));
        assert_eq!(2.001259521712195e19, dist_psmd_x4_par(&x, &y, &d));
        assert_eq!(2.001259521712195e19, dist_psmd_x4_par_dyn(&x, &y, &d));
        assert_eq!(2.0012595217121956e19, dist_wide_x4_x2(&x, &y, &d));
        assert_eq!(2.001259521712195e19, dist_x8(&x, &y, &d));
        assert_eq!(2.001259521712195e19, dist_x8_dyn(&x, &y, &d));
        assert_eq!(2.001259521712195e19, dist_x8_par(&x, &y, &d));
        assert_eq!(2.001259521712195e19, dist_x8_par_dyn(&x, &y, &d));

        /* diverges */
        let (x, y, d) = mk_inputs(42);
        assert_eq!(3.619496636926987e19, dist_naive(&x, &y, &d));
        assert_eq!(3.619496636926987e19, dist_naive_dyn(&x, &y, &d));
        assert_eq!(3.6194966369269866e19, dist_naive_par(&x, &y, &d));
        assert_eq!(3.6194966369269866e19, dist_naive_par_dyn(&x, &y, &d));
        assert_eq!(3.6194966369269866e19, dist_wide_x4(&x, &y, &d));
        assert_eq!(3.6194966369269866e19, dist_wide_x4_dyn(&x, &y, &d));
        assert_eq!(3.619496636926987e19, dist_wide_x4_par(&x, &y, &d));
        assert_eq!(3.619496636926987e19, dist_wide_x4_par_dyn(&x, &y, &d));
        assert_eq!(3.6194966369269866e19, dist_psmd_x4(&x, &y, &d));
        assert_eq!(3.6194966369269866e19, dist_psmd_x4_dyn(&x, &y, &d));
        assert_eq!(3.6194966369269866e19, dist_psmd_x4_par(&x, &y, &d));
        assert_eq!(3.6194966369269866e19, dist_psmd_x4_par_dyn(&x, &y, &d));
        assert_eq!(3.6194966369269866e19, dist_wide_x4_x2(&x, &y, &d));
        assert_eq!(3.6194966369269866e19, dist_x8(&x, &y, &d));
        assert_eq!(3.6194966369269866e19, dist_x8_dyn(&x, &y, &d));
        assert_eq!(3.6194966369269866e19, dist_x8_par(&x, &y, &d));
        assert_eq!(3.6194966369269866e19, dist_x8_par_dyn(&x, &y, &d));

        /* diverges */
        let (x, y, d) = mk_inputs(1337);
        assert_eq!(1.551683393321e20, dist_naive(&x, &y, &d));
        assert_eq!(1.551683393321e20, dist_naive_dyn(&x, &y, &d));
        //assert_eq!(1.5516833933209996e20, dist_naive_par(&x, &y, &d));
        //assert_eq!(1.5516833933209996e20, dist_naive_par_dyn(&x, &y, &d));
        assert_eq!(1.5516833933209996e20, dist_wide_x4(&x, &y, &d));
        assert_eq!(1.5516833933209996e20, dist_wide_x4_dyn(&x, &y, &d));
        assert_eq!(1.5516833933209996e20, dist_wide_x4_par(&x, &y, &d));
        assert_eq!(1.5516833933209996e20, dist_wide_x4_par_dyn(&x, &y, &d));
        assert_eq!(1.5516833933209996e20, dist_psmd_x4(&x, &y, &d));
        assert_eq!(1.5516833933209996e20, dist_psmd_x4_dyn(&x, &y, &d));
        //assert_eq!(1.5516833933209993e20, dist_psmd_x4_par(&x, &y, &d));
        //assert_eq!(1.5516833933209996e20, dist_psmd_x4_par_dyn(&x, &y, &d));
        assert_eq!(1.5516833933209993e20, dist_wide_x4_x2(&x, &y, &d));
        assert_eq!(1.5516833933209993e20, dist_x8(&x, &y, &d));
        assert_eq!(1.5516833933209993e20, dist_x8_dyn(&x, &y, &d));
        assert_eq!(1.5516833933209993e20, dist_x8_par(&x, &y, &d));
        //assert_eq!(1.5516833933209993e20, dist_x8_par_dyn(&x, &y, &d));
    }
}
