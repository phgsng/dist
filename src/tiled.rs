use multiversion::multiversion;

#[allow(unused)]
#[allow(dead_code)]
#[derive(Debug, Clone, PartialEq)]
pub struct Matrix
{
    rows: usize,
    cols: usize,
    data: Vec<f64>,
}

const BLOCK_SIZE: usize = 8;

impl Matrix
{
    pub fn new_zero(rows: usize, cols: usize) -> Self
    {
        let data = vec![0.0; rows * cols];
        Self { rows, cols, data }
    }

    pub fn new(rows: usize, cols: usize, data: Vec<f64>) -> Self
    {
        assert_eq!(data.len(), rows * cols);
        let data = data.into();
        Self { rows, cols, data }
    }

    pub fn data_mut(&mut self) -> &mut [f64] { &mut self.data[..] }

    fn row(&self, i: usize) -> &[f64]
    {
        let start = i * self.cols;
        let end = start + self.cols;
        &self.data[start..end]
    }

    fn rows(&self) -> impl Iterator<Item = &[f64]>
    {
        let mut cur = &self.data[..];

        std::iter::from_fn(move || {
            if cur.len() < self.cols {
                None
            } else {
                let row = &cur[..self.cols];
                cur = &cur[self.cols..];
                Some(row)
            }
        })
    }

    fn rows_mut(&mut self) -> impl Iterator<Item = &mut [f64]>
    {
        self.data.chunks_exact_mut(self.cols)
    }

    #[inline(never)]
    pub fn mul_vec_naive(&self, v: &[f64]) -> Vec<f64>
    {
        let mut dst = vec![0.0; self.rows];
        scalar::mat_mul_vec_into_naive(self, v, &mut dst);
        dst
    }

    #[inline(never)]
    pub fn mul_vec(&self, v: &[f64]) -> Vec<f64>
    {
        let mut dst = vec![0.0; self.rows];
        scalar::mat_mul_vec_into(self, v, &mut dst);
        dst
    }

    pub fn transpose_into(&self, dst: &mut Self)
    {
        assert_eq!(self.rows, dst.cols);
        assert_eq!(dst.rows, self.cols);

        for (i, row) in self.rows().enumerate() {
            for (j, &elt) in row.iter().enumerate() {
                let idx = j * dst.cols + i;
                dst.data[idx] = elt;
            }
        }
    }

    pub fn transpose(&self) -> Self
    {
        let mut dst = Self::new_zero(self.cols, self.rows);
        self.transpose_into(&mut dst);
        dst
    }
}

pub mod scalar
{
    use super::*;

    #[multiversion(targets = "simd")]
    #[inline(always)]
    pub fn mat_mul_vec_into_naive(m: &Matrix, v: &[f64], dst: &mut [f64])
    {
        assert_eq!(m.cols, v.len(), "rows/vec length mismatch");

        for (i, row) in m.rows().enumerate() {
            for (a, b) in row.iter().zip(v.iter()) {
                dst[i] += a * b;
            }
        }
    }

    /** Matrix times a column vector. ``dst`` is assumed zeroed. */
    #[multiversion(targets = "simd")]
    #[inline(always)]
    pub fn mat_mul_vec_into(m: &Matrix, v: &[f64], dst: &mut [f64])
    {
        assert_eq!(m.rows, dst.len(), "in/out vec length mismatch");
        assert_eq!(m.cols, v.len(), "rows/vec length mismatch");

        if dst.len() == 0 || m.rows == 0 || m.cols == 0 {
            return;
        }

        let mut start = 0;
        loop {
            let end = (start + BLOCK_SIZE).min(v.len());
            let right = &v[start..end];

            for (i, row) in m.rows().enumerate() {
                let left = &row[start..end];

                for (l, r) in left.iter().zip(right.iter()) {
                    dst[i] += l * r;
                }
            }

            if end == v.len() {
                break;
            }
            start = end;
        }
    }

    #[multiversion(targets = "simd")]
    #[inline(always)]
    pub fn vec_mul_mat_into_naive(v: &[f64], m: &Matrix, dst: &mut [f64])
    {
        assert_eq!(m.cols, dst.len(), "in/out vec length mismatch");
        assert_eq!(m.rows, v.len(), "rows/vec length mismatch");

        let m_t = m.transpose();

        for (i, row) in m_t.rows().enumerate() {
            for (a, b) in row.iter().zip(v.iter()) {
                dst[i] += a * b;
            }
        }
    }

    /** Row vector times a matrix. ``dst`` is assumed zeroed. */
    #[multiversion(targets = "simd")]
    #[inline(always)]
    pub fn vec_mul_mat_into(v: &[f64], m: &Matrix, dst: &mut [f64])
    {
        assert_eq!(m.cols, dst.len(), "in/out vec length mismatch");
        assert_eq!(m.rows, v.len(), "rows/vec length mismatch");

        if dst.len() == 0 || m.rows == 0 || m.cols == 0 {
            return;
        }

        for (a, row) in v.iter().zip(m.rows()) {
            for (b, c) in row.iter().zip(dst.iter_mut()) {
                *c += a * b;
            }
        }
    }

    #[inline(always)]
    fn transpose_tile_exact<const LANES: usize>(
        src: [&[f64; LANES]; LANES],
        dst: &mut [[f64; LANES]; LANES],
    )
    {
        /* XXX: do we gain anything from unrolling this? */
        for (i, row) in dst.iter_mut().enumerate() {
            for (j, elt) in row.iter_mut().enumerate() {
                *elt = src[j][i];
            }
        }
    }

    /**
     *  a b c d -> · a · ·
     *             · b · ·
     *             · c · ·
     *             · d · ·
     */
    #[inline(always)]
    fn transpose_tile_row_exact<const LANES: usize>(
        j: usize,
        src: &[f64; LANES],
        dst: &mut [[f64; LANES]; LANES],
    )
    {
        /* XXX: do we gain anything from unrolling this? */
        dst.iter_mut().zip(src.into_iter()).for_each(|(dst, src)| {
            dst[j] = *src;
        });
    }

    /**
     *  a b · · -> · a · ·
     *             · b · ·
     *             · · · ·
     *             · · · ·
     */
    #[inline(always)]
    fn transpose_tile_row_partial<const LANES: usize>(
        j: usize,
        src: &[f64],
        dst: &mut [[f64; LANES]; LANES],
    )
    {
        debug_assert!(src.len() <= LANES);
        dst.iter_mut().zip(src.into_iter()).for_each(|(dst, src)| {
            dst[j] = *src;
        });
    }

    #[inline(always)]
    fn transpose_4x4(src: [&[f64; 4]; 4], dst: &mut [[f64; 4]; 4])
    {
        for (i, row) in dst.iter_mut().enumerate() {
            for j in 0..4 {
                row[j] = src[j][i];
            }
        }
    }

    /**
     *  a b c d -> · a · ·
     *             · b · ·
     *             · c · ·
     *             · d · ·
     */
    #[inline(always)]
    fn transpose_1x4(col: usize, src: &[f64], dst: &mut [[f64; 4]; 4])
    {
        dst.iter_mut().zip(src.iter()).for_each(|(row, &src)| row[col] = src);
    }

    /** Assumes `bt` is transposed. */
    #[inline(always)]
    fn mul_tile<const LANES: usize>(
        a: [&[f64; LANES]; LANES],
        bt: &[[f64; LANES]; LANES],
        c: &mut [&mut [f64; LANES]; LANES],
    )
    {
        for (a_row, c_row) in a.iter().zip(c.iter_mut()) {
            mul_tile_row(a_row, bt, c_row);
        }
    }

    /** Assumes `bt` is transposed. */
    #[inline(always)]
    fn mul_tile_row<const LANES: usize>(
        a_row: &[f64; LANES],
        bt: &[[f64; LANES]; LANES],
        c_row: &mut [f64; LANES],
    )
    {
        for (b_row, c) in bt.into_iter().zip(c_row.iter_mut()) {
            for (a, b) in a_row.iter().zip(b_row.iter()) {
                *c += a * b;
            }
        }
    }

    /** Assumes `bt` is transposed. */
    #[inline(always)]
    fn mul_tile_partial<const LANES: usize>(
        a: [&[f64]; LANES],
        bt: &[[f64; LANES]; LANES],
        c: &mut [&mut [f64; LANES]; LANES],
    )
    {
        for (a_row, c_row) in a.iter().zip(c.iter_mut()) {
            mul_tile_row_partial(a_row, bt, c_row);
        }
    }

    /** Assumes `bt` is transposed. */
    #[inline(always)]
    fn mul_tile_row_partial<const LANES: usize>(
        a_row: &[f64],
        bt: &[[f64; LANES]; LANES],
        c_row: &mut [f64; LANES],
    )
    {
        debug_assert!(a_row.len() < LANES);

        for (b_row, c) in bt.into_iter().zip(c_row.iter_mut()) {
            for (a, b) in a_row.iter().zip(b_row.iter()) {
                *c += a * b;
            }
        }
    }

    /** Assumes `bt` is transposed. */
    #[inline(always)]
    #[allow(unused)]
    fn mul_4x4(
        a: &[&[f64; 4]; 4],
        bt: &[&[f64; 4]; 4],
        c: &mut [&mut [f64; 4]; 4],
    )
    {
        for (a_row, c_row) in a.iter().zip(c.iter_mut()) {
            for (b_row, c) in bt.into_iter().zip(c_row.iter_mut()) {
                for (a, b) in a_row.iter().zip(b_row.iter()) {
                    *c += a * b;
                }
            }
        }
    }

    /** Assumes `bt` is transposed. */
    #[inline(always)]
    fn mul_1x4(a_row: &[f64], bt: &[[f64; 4]; 4], c_row: &mut [f64; 4])
    {
        debug_assert!(a_row.len() <= 4);
        for (b_row, c) in bt.into_iter().zip(c_row.iter_mut()) {
            for (a, b) in a_row.iter().zip(b_row.iter()) {
                *c += a * b;
            }
        }
    }

    #[inline(always)]
    fn dot_naive(v: &[f64], w: &[f64]) -> f64
    {
        assert_eq!(v.len(), w.len(), "dimension mismatch");
        v.into_iter().zip(w.into_iter()).map(|(v, w)| v * w).sum::<f64>()
    }

    pub fn matmul_naive_into(a: &Matrix, b: &Matrix, c: &mut Matrix)
    {
        assert_eq!(a.cols, b.rows, "dimension mismatch");
        assert_eq!(a.rows, c.rows, "dimension mismatch");
        assert_eq!(b.cols, c.cols, "dimension mismatch");

        let bt = b.transpose();
        matmul_with_transpose_into(a, &bt, c);
    }

    /** Assumes that *b* is actually its transpose. */
    fn matmul_with_transpose_into(a: &Matrix, b: &Matrix, c: &mut Matrix)
    {
        c.rows_mut().enumerate().for_each(|(i, row)| {
            row.iter_mut().enumerate().for_each(|(j, c)| {
                let a = a.row(i);
                let b = b.row(j);

                *c = dot_naive(a, b);
            });
        });
    }

    pub fn matmul_into(a: &Matrix, b: &Matrix, c: &mut Matrix)
    {
        assert_eq!(a.cols, b.rows, "dimension mismatch");
        assert_eq!(a.rows, c.rows, "dimension mismatch");
        assert_eq!(b.cols, c.cols, "dimension mismatch");

        /* 4 rows in the output C */
        let mut c_rows = c.data.chunks_mut(c.cols);
        let mut a_rows = a.data.chunks(a.cols);

        loop {
            //eprintln!("##################################");
            /* batch 4 output rows per outer loop */
            /* XXX: these could be unchecked */
            let Some(c0) = c_rows.next() else { break };
            let c1 = c_rows.next();
            let c2 = c_rows.next();
            let c3 = c_rows.next();

            /* accumulate in blocks of 4x4 */
            let mut c0 = c0.chunks_mut(4);
            let mut c1 = c1.map(|c1| c1.chunks_mut(4));
            let mut c2 = c2.map(|c2| c2.chunks_mut(4));
            let mut c3 = c3.map(|c3| c3.chunks_mut(4));

            let a0 = a_rows.next().unwrap();
            let a1 = a_rows.next();
            let a2 = a_rows.next();
            let a3 = a_rows.next();

            debug_assert_eq!(a1.is_some(), c1.is_some());
            debug_assert_eq!(a2.is_some(), c2.is_some());
            debug_assert_eq!(a3.is_some(), c3.is_some());

            let mut i = 0;

            loop {
                let Some(c0) = c0.next() else { break };
                let c1: Option<&mut [f64]> =
                    c1.as_mut().map(|c1| c1.next().unwrap());
                let c2: Option<&mut [f64]> =
                    c2.as_mut().map(|c2| c2.next().unwrap());
                let c3: Option<&mut [f64]> =
                    c3.as_mut().map(|c3| c3.next().unwrap());

                //eprintln!("{} ===============================", i);

                let mut a0 = a0.chunks(4);
                let mut a1 = a1.map(|a1| a1.chunks(4));
                let mut a2 = a2.map(|a2| a2.chunks(4));
                let mut a3 = a3.map(|a3| a3.chunks(4));

                if c0.len() == 4 {
                    let c0: &mut [f64; 4] = c0.try_into().unwrap();
                    let mut c1: Option<&mut [f64; 4]> =
                        c1.map(|c1| c1.try_into().unwrap());
                    let mut c2: Option<&mut [f64; 4]> =
                        c2.map(|c2| c2.try_into().unwrap());
                    let mut c3: Option<&mut [f64; 4]> =
                        c3.map(|c3| c3.try_into().unwrap());

                    let mut b_rows = b.data.chunks(b.cols);

                    loop {
                        //eprintln!("----------------------------------");
                        let b4x4 = {
                            let Some(b0) = b_rows.next() else { break };
                            let b1 = b_rows.next();
                            let b2 = b_rows.next();
                            let b3 = b_rows.next();

                            let b_off = i * 4;

                            let none = [0.0; 4];

                            let b0: &[f64; 4] =
                                b0[b_off..b_off + 4].try_into().unwrap();
                            let b1: &[f64; 4] = b1
                                .map(|b1| {
                                    b1[b_off..b_off + 4].try_into().unwrap()
                                })
                                .unwrap_or(&none);
                            let b2: &[f64; 4] = b2
                                .map(|b1| {
                                    b1[b_off..b_off + 4].try_into().unwrap()
                                })
                                .unwrap_or(&none);
                            let b3: &[f64; 4] = b3
                                .map(|b1| {
                                    b1[b_off..b_off + 4].try_into().unwrap()
                                })
                                .unwrap_or(&none);

                            //eprintln!("b0: {:?}", b0);
                            //eprintln!("b1: {:?}", b1);
                            //eprintln!("b2: {:?}", b2);
                            //eprintln!("b3: {:?}", b3);

                            let mut b4x4 = [[0.0; 4]; 4];
                            transpose_4x4([&b0, &b1, &b2, &b3], &mut b4x4);
                            b4x4
                        };

                        let a = a0.next().unwrap();
                        mul_1x4(a, &b4x4, c0);

                        if let Some(a1) = a1.as_mut() {
                            let a = a1.next().unwrap();
                            mul_1x4(a, &b4x4, c1.as_mut().unwrap());

                            if let Some(a2) = a2.as_mut() {
                                let a = a2.next().unwrap();
                                mul_1x4(a, &b4x4, c2.as_mut().unwrap());

                                if let Some(a3) = a3.as_mut() {
                                    let a = a3.next().unwrap();
                                    mul_1x4(a, &b4x4, c3.as_mut().unwrap());
                                }
                            }
                        }

                        //eprintln!("··································");
                        //eprintln!("c0: {:?}", c0);
                        //eprintln!("c1: {:?}", c1);
                        //eprintln!("c2: {:?}", c2);
                        //eprintln!("c3: {:?}", c3);
                    }
                } else {
                    /* less than 4 horizontal elements of c */
                    #[inline(always)]
                    fn get4(xs: &[f64]) -> [f64; 4]
                    {
                        let mut res = [0.0; 4];
                        (&mut res[..xs.len()]).copy_from_slice(&xs);
                        res
                    }

                    let mut sc0 = get4(c0);
                    let sc1 = c1.as_ref().map(|c1| get4(c1));
                    let sc2 = c2.as_ref().map(|c2| get4(c2));
                    let sc3 = c3.as_ref().map(|c3| get4(c3));

                    let mut b_rows = b.data.chunks(b.cols);

                    loop {
                        //eprintln!("----------------------------------");
                        let b4x4 = {
                            let Some(b0) = b_rows.next() else { break };
                            let b1 = b_rows.next();
                            let b2 = b_rows.next();
                            let b3 = b_rows.next();

                            let b_off = i * 4;

                            let mut b4x4 = [[0.0; 4]; 4];

                            transpose_1x4(
                                0,
                                &b0[b_off..b_off + c0.len()],
                                &mut b4x4,
                            );
                            if let Some(b1) = b1 {
                                transpose_1x4(
                                    1,
                                    &b1[b_off..b_off + c0.len()],
                                    &mut b4x4,
                                )
                            };
                            if let Some(b2) = b2 {
                                transpose_1x4(
                                    2,
                                    &b2[b_off..b_off + c0.len()],
                                    &mut b4x4,
                                )
                            };
                            if let Some(b3) = b3 {
                                transpose_1x4(
                                    3,
                                    &b3[b_off..b_off + c0.len()],
                                    &mut b4x4,
                                )
                            };

                            b4x4
                        };

                        let a = a0.next().unwrap();
                        mul_1x4(a, &b4x4, &mut sc0);

                        if let Some(a1) = &mut a1 {
                            let a = a1.next().unwrap();
                            mul_1x4(a, &b4x4, &mut sc1.unwrap());

                            if let Some(a2) = &mut a2 {
                                let a = a2.next().unwrap();
                                mul_1x4(a, &b4x4, &mut sc2.unwrap());

                                if let Some(a3) = &mut a3 {
                                    let a = a3.next().unwrap();
                                    mul_1x4(a, &b4x4, &mut sc3.unwrap());
                                }
                            }
                        }

                        //eprintln!("sc0: {:?}", sc0);
                        //eprintln!("sc1: {:?}", sc1);
                        //eprintln!("sc2: {:?}", sc2);
                        //eprintln!("sc3: {:?}", sc3);
                    }

                    c0.copy_from_slice(&mut sc0[..c0.len()]);
                    c1.map(|c1| {
                        c1.copy_from_slice(&mut sc1.unwrap()[..c0.len()])
                    });
                    c2.map(|c2| {
                        c2.copy_from_slice(&mut sc2.unwrap()[..c0.len()])
                    });
                    c3.map(|c3| {
                        c3.copy_from_slice(&mut sc3.unwrap()[..c0.len()])
                    });
                }

                i += 1;
            }
        }
    }

    #[inline(always)]
    fn mat_mul_block_exact<const LANES: usize>(
        cc: &mut [&mut [f64; LANES]; LANES],
        aa: &Matrix,
        bb: &Matrix,
        row_chunk: usize,
        col_chunk: usize,
    )
    {
        use std::slice::Chunks;

        /* `LANES` rows starting at row `row_chunk`. */
        let mut a: [Chunks<f64>; LANES] = {
            let aa = &mut aa.data[row_chunk * aa.cols..].chunks_exact(aa.cols);
            array_init::array_init(|_| aa.next().unwrap().chunks(LANES))
        };

        let col_start = col_chunk;
        let col_end = col_start + LANES;
        let mut bb = bb.rows();

        /* Store the transpose of our b chunk on the stack. */
        let mut bt: [[f64; LANES]; LANES] = [[0.0; LANES]; LANES];

        for _ in (0..aa.cols).step_by(LANES) {
            let a: [&[f64]; LANES] =
                array_init::array_init(|n| a[n].next().unwrap());
            let n = a[0].len();

            if n == LANES {
                let a: [&[f64; LANES]; LANES] =
                    a.map(|a| a.try_into().unwrap());

                let b: [&[f64; LANES]; LANES] = array_init::array_init(|_| {
                    let b: &[f64] = bb.next().unwrap();
                    let b: &[f64; LANES] =
                        b[col_start..col_end].try_into().unwrap();
                    b
                });

                transpose_tile_exact(b, &mut bt);
                mul_tile(a, &bt, cc);
            } else {
                assert!(n < LANES);

                for col in 0..n {
                    let b: &[f64] = bb.next().unwrap();
                    let b = &b[col_start..col_end];
                    let b: &[f64; LANES] = b.try_into().unwrap();
                    transpose_tile_row_exact(col, b, &mut bt);
                }

                /* XXX: probably slower than a memset before transposing */
                for col in n..LANES {
                    for m in 0..LANES {
                        bt[m][col] = 0.0;
                    }
                }

                mul_tile_partial(a, &bt, cc);
            }
        }
    }

    #[inline(always)]
    fn mat_mul_block_partial<const LANES: usize>(
        /* The inputs can be any geometry up to ``LANES``. */
        cc: &mut [&mut [f64]],
        aa: &Matrix,
        bb: &Matrix,
        row_chunk: usize,
        col_chunk: usize,
    )
    {
        use heapless::Vec;
        use std::slice::Chunks;

        let n = cc.len();
        assert!(0 < n);
        assert!(n <= LANES);

        let m = cc[0].len();
        assert!(0 < m);
        assert!(m <= LANES);

        /* not technically needed but we check anyways to prevent mistakes */
        assert!(m < LANES || n < LANES);

        let mut sc = [[0.0; LANES]; LANES];

        /* Load values from C into our scratch space. */
        cc.iter()
            .zip(sc.iter_mut())
            .for_each(|(c, sc)| (&mut sc[..c.len()]).copy_from_slice(c));

        let mut b_rows = bb.data.chunks(bb.cols);

        let mut a_chunks: Vec<Chunks<f64>, LANES> = aa.data
            [row_chunk * aa.cols..row_chunk * aa.cols + n * aa.cols]
            .chunks_exact(aa.cols)
            .map(|a_row| a_row.chunks(LANES))
            .collect();

        for _ in (0..bb.rows).step_by(LANES) {
            let bt = {
                let mut bt = [[0.0; LANES]; LANES];

                b_rows.by_ref().take(LANES).enumerate().for_each(
                    |(ii, row)| {
                        transpose_tile_row_partial::<LANES>(
                            ii,
                            &row[col_chunk..col_chunk + m],
                            &mut bt,
                        );
                    },
                );

                bt
            };

            let mut a_tmp = [0.0; LANES];

            for (c_chunk, a_row) in sc.iter_mut().zip(a_chunks.iter_mut()) {
                let a_chunk = a_row.next().unwrap();
                debug_assert!(a_chunk.len() <= LANES);
                (&mut a_tmp[..a_chunk.len()]).copy_from_slice(a_chunk);
                mul_tile_row(&a_tmp, &bt, c_chunk);
            }
        }

        /* Replace values in C with results. */
        cc.iter_mut()
            .zip(sc.iter())
            .for_each(|(c, sc)| c.copy_from_slice(&sc[..c.len()]));
    }

    pub fn matmul_branching_into<const LANES: usize>(
        a: &Matrix,
        b: &Matrix,
        c: &mut Matrix,
    )
    {
        use heapless::Vec;

        //eprintln!(
        //"[{}] »» multiply, A={}x{} · B={}x{}, C={}x{}",
        //LANES, a.rows, a.cols, b.rows, b.cols, c.rows, c.cols,
        //);

        use std::slice::ChunksMut;

        assert_eq!(a.cols, b.rows, "dimension mismatch");
        assert_eq!(a.rows, c.rows, "dimension mismatch");
        assert_eq!(b.cols, c.cols, "dimension mismatch");

        let mut c_rows = c.data.chunks_exact_mut(c.cols);

        for row_chunk in (0..c.rows).step_by(LANES) {
            //eprintln!("[{}] »»» row={}/{}", LANES, row_chunk, c.rows);

            let mut c_rows: Vec<&mut [f64], { LANES }> =
                c_rows.by_ref().take(LANES).collect();

            if c.rows >= row_chunk + LANES {
                //eprintln!("[{}] »»»» full chunk", LANES);

                /* Traversing *LANES* rows in chunks of *LANES* length. */
                let mut c_rows: [ChunksMut<f64>; LANES] = {
                    let mut c = c_rows.iter_mut();
                    array_init::array_init(|_| {
                        c.next().unwrap().chunks_mut(LANES)
                    })
                };

                for col_chunk in (0..c.cols).step_by(LANES) {
                    let mut c: [&mut [f64]; LANES] = {
                        let mut c = c_rows.iter_mut();
                        array_init::array_init(|_| {
                            let c_chunks = c.next().unwrap();
                            c_chunks.next().unwrap()
                        })
                    };

                    //eprintln!("[{}] »»»» multiplying into chunk: {:?}", LANES, &c);

                    if c[0].len() == LANES {
                        let mut c: [&mut [f64; LANES]; LANES] =
                            c.map(|c| c.try_into().unwrap());

                        mat_mul_block_exact::<LANES>(
                            &mut c, a, b, row_chunk, col_chunk,
                        );
                    } else {
                        /* less than ``LANES`` horizontal elements of c */
                        mat_mul_block_partial::<LANES>(
                            &mut c, a, b, row_chunk, col_chunk,
                        );
                    }
                }
            } else {
                /* XXX couldn’t we use heapless everywhere? */

                //let n_rows = c.rows - row_chunk;
                //eprintln!("[{}] »»»» chunk partial, {} rows", LANES, n_rows);

                let mut c_rows: Vec<ChunksMut<f64>, LANES> = c_rows
                    .iter_mut()
                    .map(|c_rows| c_rows.chunks_mut(LANES))
                    .collect();

                for col_chunk in (0..c.cols).step_by(LANES) {
                    let mut c: heapless::Vec<&mut [f64], LANES> = c_rows
                        .iter_mut()
                        .map(|row| row.next().unwrap())
                        .collect();

                    //eprintln!("[{}] »»»» multiplying into chunk: {:?}", LANES, &c);

                    /* less than ``LANES`` horizontal elements of c */
                    mat_mul_block_partial::<LANES>(
                        &mut c, a, b, row_chunk, col_chunk,
                    );
                }
                /* This branch only always taken in the last iteration but the borrow
                 * checker doesn’t know that unless we make it explicit. */
                break;
            }
        }
    }
}

pub mod simd
{
    use super::*;

    use packed_simd::{f64x4, f64x8, m64x4, m64x8, Simd, SimdArray};

    pub mod util
    {
        use super::*;

        use packed_simd::{f64x4, f64x8, m64x4, m64x8};

        #[inline(always)]
        fn transpose_tile_exact_xN<E, S, const LANES: usize>(
            src: [&[E; LANES]; LANES],
            dst: &mut [S; LANES],
        ) where
            E: Copy + Clone,
            S: From<[E; LANES]>,
        {
            for (i, row) in dst.iter_mut().enumerate() {
                let tmp: [E; LANES] = array_init::array_init(|j| src[j][i]);
                *row = S::from(tmp);
            }
        }

        /**
         *  a b c d -> · a · ·
         *             · b · ·
         *             · c · ·
         *             · d · ·
         */
        #[inline(always)]
        fn transpose_tile_row_exact<const LANES: usize>(
            j: usize,
            src: &[f64; LANES],
            dst: &mut [[f64; LANES]; LANES],
        )
        {
            /* XXX: do we gain anything from unrolling this? */
            dst.iter_mut().zip(src.into_iter()).for_each(|(dst, src)| {
                dst[j] = *src;
            });
        }

        /**
         *  a b · · -> · a · ·
         *             · b · ·
         *             · · · ·
         *             · · · ·
         */
        #[inline(always)]
        fn transpose_tile_row_partial<const LANES: usize>(
            j: usize,
            src: &[f64],
            dst: &mut [[f64; LANES]; LANES],
        )
        {
            debug_assert!(src.len() <= LANES);
            dst.iter_mut().zip(src.into_iter()).for_each(|(dst, src)| {
                dst[j] = *src;
            });
        }

        #[cfg(test)]
        mod test
        {
            use super::*;

            #[test]
            fn transpose_tile()
            {
                let src: [[f64; 8]; 8] = [
                    [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0],
                    [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0],
                    [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0],
                    [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0],
                    [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0],
                    [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0],
                    [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0],
                    [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0],
                ];
                let src_t = {
                    let src: [&[f64; 8]; 8] =
                        array_init::array_init(|n| &src[n]);
                    let mut tmp: [f64x8; 8] =
                        array_init::array_init(|_| f64x8::default());
                    transpose_tile_exact_xN::<f64, f64x8, 8>(src, &mut tmp);
                    tmp
                };

                let result: [[f64; 8]; 8] =
                    array_init::array_init(|n| src_t[n].into());
                let expect: [[f64; 8]; 8] = [
                    [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
                    [2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0],
                    [3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0],
                    [4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0],
                    [5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0],
                    [6.0, 6.0, 6.0, 6.0, 6.0, 6.0, 6.0, 6.0],
                    [7.0, 7.0, 7.0, 7.0, 7.0, 7.0, 7.0, 7.0],
                    [8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0],
                ];
                assert_eq!(result, expect);
            }
        }
    }
}

#[cfg(test)]
mod ops
{
    use super::*;

    #[test]
    fn transpose()
    {
        #[rustfmt::skip]
        let m = Matrix::new(2, 2,
            vec![1.0, 2.0,
                 3.0, 4.0]);
        #[rustfmt::skip]
        assert_eq!(m.transpose(), Matrix::new(2, 2, vec![
                1.0, 3.0,
                2.0, 4.0,
        ]));
        assert_eq!(m, m.transpose().transpose());
    }
}

#[cfg(test)]
mod mat_vec
{
    use super::*;

    #[test]
    fn all_zero_ok()
    {
        let m = Matrix::new_zero(4, 2);

        assert_eq!(m.mul_vec(&[0.0, 0.0]), vec![0.0; 4]);
        assert_eq!(m.mul_vec(&[1.2, 3.4]), vec![0.0; 4]);
    }

    #[test]
    fn mul_ok()
    {
        assert_eq!(
            Matrix::new(3, 2, [1.0, 1.0, 1.0, 1.0, 1.0, 1.0].into())
                .mul_vec(&[0.0, 0.0]),
            vec![0.0; 3]
        );
        assert_eq!(
            Matrix::new(3, 2, [1.0, 1.0, 1.0, 1.0, 1.0, 1.0].into())
                .mul_vec(&[1.2, 3.4]),
            &[4.6, 4.6, 4.6]
        );
        assert_eq!(
            Matrix::new(3, 2, [1.0, 2.0, 3.0, 4.0, 5.0, 6.0].into())
                .mul_vec(&[1.0, 2.0]),
            &[5.0, 11.0, 17.0]
        );
        assert_eq!(
            Matrix::new(3, 2, [1.0, 1.0, 1.0, 1.0, 1.0, 1.0].into())
                .mul_vec(&[0.0, 0.0]),
            Matrix::new(3, 2, [1.0, 1.0, 1.0, 1.0, 1.0, 1.0].into())
                .mul_vec_naive(&[0.0, 0.0]),
        );
        assert_eq!(
            Matrix::new(3, 2, [1.0, 1.0, 1.0, 1.0, 1.0, 1.0].into())
                .mul_vec(&[1.2, 3.4]),
            Matrix::new(3, 2, [1.0, 1.0, 1.0, 1.0, 1.0, 1.0].into())
                .mul_vec_naive(&[1.2, 3.4]),
        );
        assert_eq!(
            Matrix::new(3, 2, [1.0, 2.0, 3.0, 4.0, 5.0, 6.0].into())
                .mul_vec(&[1.0, 2.0]),
            Matrix::new(3, 2, [1.0, 2.0, 3.0, 4.0, 5.0, 6.0].into())
                .mul_vec_naive(&[1.0, 2.0]),
        );
    }

    #[test]
    #[should_panic]
    fn dimens_panic()
    {
        let m = Matrix::new_zero(4, 2);
        assert_eq!(m.mul_vec(&[1.2]), vec![0.0; 4]);
    }
}

#[cfg(test)]
mod mat_mat_mul
{
    use super::*;

    fn test_mul_fun(
        mul: fn(a: &Matrix, b: &Matrix, c: &mut Matrix),
        a: &Matrix,
        b: &Matrix,
        expect: &Matrix,
    ) -> bool
    {
        let mut c = Matrix::new_zero(a.rows, b.cols);
        mul(a, b, &mut c);
        c == *expect
    }

    #[test]
    fn square_2x2_ok()
    {
        #[rustfmt::skip]
        let a = Matrix::new(2, 2, vec![
            1.0, 0.0,
            0.0, 0.0,
        ]);

        #[rustfmt::skip]
        let b = Matrix::new(2, 2, vec![
            1.0, 1.0,
            1.0, 1.0,
        ]);

        #[rustfmt::skip]
        let expect = Matrix::new(2, 2, vec![
                1.0, 1.0,
                0.0, 0.0,
        ]);

        assert!(test_mul_fun(scalar::matmul_into, &a, &b, &expect));
        assert!(test_mul_fun(scalar::matmul_naive_into, &a, &b, &expect));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<2>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<4>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<8>,
            &a,
            &b,
            &expect
        ));
    }

    #[test]
    fn square_4x4_ok()
    {
        #[rustfmt::skip]
        let a = Matrix::new(4, 4, vec![
            1.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0,
        ]);

        #[rustfmt::skip]
        let b = Matrix::new(4, 4, vec![
            1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0,
        ]);

        #[rustfmt::skip]
        let expect = Matrix::new(4, 4, vec![
                1.0, 1.0, 1.0, 1.0,
                0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0,
        ]);

        assert!(test_mul_fun(scalar::matmul_into, &a, &b, &expect));
        assert!(test_mul_fun(scalar::matmul_naive_into, &a, &b, &expect));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<2>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<4>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<8>,
            &a,
            &b,
            &expect
        ));
    }

    #[test]
    fn square_4x4_stripes_ok()
    {
        #[rustfmt::skip]
        let a = Matrix::new(4, 4, vec![
            1.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0,
            2.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0,
        ]);

        #[rustfmt::skip]
        let b = Matrix::new(4, 4, vec![
            1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0,
        ]);

        #[rustfmt::skip]
        let expect = Matrix::new(4, 4, vec![
                1.0, 1.0, 1.0, 1.0,
                0.0, 0.0, 0.0, 0.0,
                2.0, 2.0, 2.0, 2.0,
                0.0, 0.0, 0.0, 0.0,
        ]);

        assert!(test_mul_fun(scalar::matmul_into, &a, &b, &expect));
        assert!(test_mul_fun(scalar::matmul_naive_into, &a, &b, &expect));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<2>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<4>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<8>,
            &a,
            &b,
            &expect
        ));
    }

    #[test]
    fn square_4x4_diag_ascend_ok()
    {
        #[rustfmt::skip]
        let a = Matrix::new(4, 4, vec![
            1.0, 0.0, 0.0, 0.0,
            0.0, 2.0, 0.0, 0.0,
            0.0, 0.0, 3.0, 0.0,
            0.0, 0.0, 0.0, 4.0,
        ]);

        #[rustfmt::skip]
        let b = Matrix::new(4, 4, vec![
            1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0,
        ]);
        let mut c = Matrix::new_zero(4, 4);

        scalar::matmul_into(&a, &b, &mut c);

        #[rustfmt::skip]
        let expect = Matrix::new(4, 4, vec![
                1.0, 1.0, 1.0, 1.0,
                2.0, 2.0, 2.0, 2.0,
                3.0, 3.0, 3.0, 3.0,
                4.0, 4.0, 4.0, 4.0,
        ]);

        assert!(test_mul_fun(scalar::matmul_into, &a, &b, &expect));
        assert!(test_mul_fun(scalar::matmul_naive_into, &a, &b, &expect));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<2>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<4>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<8>,
            &a,
            &b,
            &expect
        ));
    }

    #[test]
    fn rectang_stripes_ok()
    {
        #[rustfmt::skip]
        let a = Matrix::new(8, 4, vec![
            1.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0,
            2.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0,
        ]);

        #[rustfmt::skip]
        let b = Matrix::new(4, 8, vec![
            1.0, 1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0,
            2.0, 2.0, 2.0, 2.0, -2.0, -2.0, -2.0, -2.0,
            3.0, 3.0, 3.0, 3.0, -3.0, -3.0, -3.0, -3.0,
            4.0, 4.0, 4.0, 4.0, -4.0, -4.0, -4.0, -4.0,
        ]);

        #[rustfmt::skip]
        let expect = Matrix::new(8, 8, vec![
                1.0, 1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0,
                0.0, 0.0, 0.0, 0.0,  0.0,  0.0,  0.0,  0.0,
                0.0, 0.0, 0.0, 0.0,  0.0,  0.0,  0.0,  0.0,
                0.0, 0.0, 0.0, 0.0,  0.0,  0.0,  0.0,  0.0,
                2.0, 2.0, 2.0, 2.0, -2.0, -2.0, -2.0, -2.0,
                0.0, 0.0, 0.0, 0.0,  0.0,  0.0,  0.0,  0.0,
                0.0, 0.0, 0.0, 0.0,  0.0,  0.0,  0.0,  0.0,
                0.0, 0.0, 0.0, 0.0,  0.0,  0.0,  0.0,  0.0,
        ]);

        assert!(test_mul_fun(scalar::matmul_into, &a, &b, &expect));
        assert!(test_mul_fun(scalar::matmul_naive_into, &a, &b, &expect));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<2>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<4>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<8>,
            &a,
            &b,
            &expect
        ));
    }

    #[test]
    fn square_8x8()
    {
        #[rustfmt::skip]
        let a = Matrix::new(8, 8, vec![
            1.0, 0.0, 0.0, 0.0,  0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0,  0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0,  0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0,  0.0, 0.0, 0.0, 0.0,

            2.0, 0.0, 0.0, 0.0,  0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0,  0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0,  0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0,  0.0, 0.0, 0.0, 0.0,
        ]);

        #[rustfmt::skip]
        let b = Matrix::new(8, 8, vec![
            1.0, 1.0, 1.0, 1.0,  -1.0, -1.0, -1.0, -1.0,
            2.0, 2.0, 2.0, 2.0,  -2.0, -2.0, -2.0, -2.0,
            3.0, 3.0, 3.0, 3.0,  -3.0, -3.0, -3.0, -3.0,
            4.0, 4.0, 4.0, 4.0,  -4.0, -4.0, -4.0, -4.0,

            1.0, 1.0, 1.0, 1.0,  -1.0, -1.0, -1.0, -1.0,
            2.0, 2.0, 2.0, 2.0,  -2.0, -2.0, -2.0, -2.0,
            3.0, 3.0, 3.0, 3.0,  -3.0, -3.0, -3.0, -3.0,
            4.0, 4.0, 4.0, 4.0,  -4.0, -4.0, -4.0, -4.0,
        ]);

        #[rustfmt::skip]
        let expect = Matrix::new(8, 8, vec![
                1.0, 1.0, 1.0, 1.0,  -1.0, -1.0, -1.0, -1.0,
                0.0, 0.0, 0.0, 0.0,   0.0,  0.0,  0.0,  0.0,
                0.0, 0.0, 0.0, 0.0,   0.0,  0.0,  0.0,  0.0,
                0.0, 0.0, 0.0, 0.0,   0.0,  0.0,  0.0,  0.0,

                2.0, 2.0, 2.0, 2.0,  -2.0, -2.0, -2.0, -2.0,
                0.0, 0.0, 0.0, 0.0,   0.0,  0.0,  0.0,  0.0,
                0.0, 0.0, 0.0, 0.0,   0.0,  0.0,  0.0,  0.0,
                0.0, 0.0, 0.0, 0.0,   0.0,  0.0,  0.0,  0.0,
        ]);

        assert!(test_mul_fun(scalar::matmul_into, &a, &b, &expect));
        assert!(test_mul_fun(scalar::matmul_naive_into, &a, &b, &expect));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<2>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<4>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<8>,
            &a,
            &b,
            &expect
        ));
    }

    #[test]
    fn square_9x9()
    {
        #[rustfmt::skip]
        let a = Matrix::new(9, 9, vec![
            1.0, 0.0, 0.0, 0.0,  0.0, 0.0, 0.0, 0.0,  0.0,
            0.0, 0.0, 0.0, 0.0,  0.0, 0.0, 0.0, 0.0,  0.0,
            0.0, 0.0, 0.0, 0.0,  0.0, 0.0, 0.0, 0.0,  0.0,
            0.0, 0.0, 0.0, 0.0,  0.0, 0.0, 0.0, 0.0,  0.0,

            2.0, 0.0, 0.0, 0.0,  0.0, 0.0, 0.0, 0.0,  0.0,
            0.0, 0.0, 0.0, 0.0,  0.0, 0.0, 0.0, 0.0,  0.0,
            0.0, 0.0, 0.0, 0.0,  0.0, 0.0, 0.0, 0.0,  0.0,
            0.0, 0.0, 0.0, 0.0,  0.0, 0.0, 0.0, 0.0,  0.0,

            3.0, 0.0, 0.0, 0.0,  0.0, 0.0, 0.0, 0.0,  0.0,
        ]);

        #[rustfmt::skip]
        let b = Matrix::new(9, 9, vec![
            1.0, 1.0, 1.0, 1.0,  -1.0, -1.0, -1.0, -1.0,  1.0,
            2.0, 2.0, 2.0, 2.0,  -2.0, -2.0, -2.0, -2.0,  2.0,
            3.0, 3.0, 3.0, 3.0,  -3.0, -3.0, -3.0, -3.0,  3.0,
            4.0, 4.0, 4.0, 4.0,  -4.0, -4.0, -4.0, -4.0,  4.0,

            1.0, 1.0, 1.0, 1.0,  -1.0, -1.0, -1.0, -1.0,  1.0,
            2.0, 2.0, 2.0, 2.0,  -2.0, -2.0, -2.0, -2.0,  2.0,
            3.0, 3.0, 3.0, 3.0,  -3.0, -3.0, -3.0, -3.0,  3.0,
            4.0, 4.0, 4.0, 4.0,  -4.0, -4.0, -4.0, -4.0,  4.0,

            5.0, 5.0, 5.0, 5.0,  -5.0, -5.0, -5.0, -5.0,  5.0,
        ]);

        let mut c = Matrix::new_zero(9, 9);

        scalar::matmul_into(&a, &b, &mut c);

        #[rustfmt::skip]
        let expect = Matrix::new(9, 9, vec![
                1.0, 1.0, 1.0, 1.0,  -1.0, -1.0, -1.0, -1.0,  1.0,
                0.0, 0.0, 0.0, 0.0,   0.0,  0.0,  0.0,  0.0,  0.0,
                0.0, 0.0, 0.0, 0.0,   0.0,  0.0,  0.0,  0.0,  0.0,
                0.0, 0.0, 0.0, 0.0,   0.0,  0.0,  0.0,  0.0,  0.0,

                2.0, 2.0, 2.0, 2.0,  -2.0, -2.0, -2.0, -2.0,  2.0,
                0.0, 0.0, 0.0, 0.0,   0.0,  0.0,  0.0,  0.0,  0.0,
                0.0, 0.0, 0.0, 0.0,   0.0,  0.0,  0.0,  0.0,  0.0,
                0.0, 0.0, 0.0, 0.0,   0.0,  0.0,  0.0,  0.0,  0.0,

                3.0, 3.0, 3.0, 3.0,  -3.0, -3.0, -3.0, -3.0,  3.0,
        ]);

        assert!(test_mul_fun(scalar::matmul_into, &a, &b, &expect));
        assert!(test_mul_fun(scalar::matmul_naive_into, &a, &b, &expect));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<2>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<4>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<8>,
            &a,
            &b,
            &expect
        ));
    }

    #[test]
    fn rect_9x11()
    {
        #[rustfmt::skip]
        let a = Matrix::new(9, 5, vec![
            1.0, 0.0, 0.0, 0.0,  0.0,
            0.0, 0.0, 0.0, 0.0,  0.0,
            0.0, 0.0, 0.0, 0.0,  0.0,
            0.0, 0.0, 0.0, 0.0,  0.0,
                                     
            2.0, 0.0, 0.0, 0.0,  0.0,
            0.0, 0.0, 0.0, 0.0,  0.0,
            0.0, 0.0, 0.0, 0.0,  0.0,
            0.0, 0.0, 0.0, 0.0,  0.0,
                                     
            3.0, 0.0, 0.0, 0.0,  0.0,
        ]);

        #[rustfmt::skip]
        let b = Matrix::new(5, 11, vec![
            1.0, 1.0, 1.0, 1.0,  -1.0, -1.0, -1.0, -1.0,  1.0, 1.0, 1.0,
            2.0, 2.0, 2.0, 2.0,  -2.0, -2.0, -2.0, -2.0,  2.0, 2.0, 2.0,
            3.0, 3.0, 3.0, 3.0,  -3.0, -3.0, -3.0, -3.0,  3.0, 3.0, 3.0,
            4.0, 4.0, 4.0, 4.0,  -4.0, -4.0, -4.0, -4.0,  4.0, 4.0, 4.0,
                                                                   
            1.0, 1.0, 1.0, 1.0,  -1.0, -1.0, -1.0, -1.0,  1.0, 1.0, 1.0,
        ]);

        #[rustfmt::skip]
        let expect = Matrix::new(9, 11, vec![
                1.0, 1.0, 1.0, 1.0,  -1.0, -1.0, -1.0, -1.0,  1.0, 1.0, 1.0,
                0.0, 0.0, 0.0, 0.0,   0.0,  0.0,  0.0,  0.0,  0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0,   0.0,  0.0,  0.0,  0.0,  0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0,   0.0,  0.0,  0.0,  0.0,  0.0, 0.0, 0.0,
                                                                       
                2.0, 2.0, 2.0, 2.0,  -2.0, -2.0, -2.0, -2.0,  2.0, 2.0, 2.0,
                0.0, 0.0, 0.0, 0.0,   0.0,  0.0,  0.0,  0.0,  0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0,   0.0,  0.0,  0.0,  0.0,  0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0,   0.0,  0.0,  0.0,  0.0,  0.0, 0.0, 0.0,
                                                                       
                3.0, 3.0, 3.0, 3.0,  -3.0, -3.0, -3.0, -3.0,  3.0, 3.0, 3.0,
        ]);

        assert!(test_mul_fun(scalar::matmul_into, &a, &b, &expect));
        assert!(test_mul_fun(scalar::matmul_naive_into, &a, &b, &expect));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<2>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<4>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<8>,
            &a,
            &b,
            &expect
        ));
    }

    #[test]
    fn remainder_4x5()
    {
        #[rustfmt::skip]
        let a = Matrix::new(4, 5, vec![
            1.0, 0.0, 0.0, 0.0,  1.0,
            0.0, 0.0, 0.0, 0.0,  0.0,
            0.0, 1.0, 0.0, 0.0,  0.0,
            0.0, 0.0, 0.0, 0.0,  0.0,
        ]);

        #[rustfmt::skip]
        let b = Matrix::new(5, 4, vec![
            1.0, 1.0, 1.0, 1.0,
            2.0, 2.0, 2.0, 2.0,
            3.0, 3.0, 3.0, 3.0,
            4.0, 4.0, 4.0, 4.0,

            5.0, 5.0, 5.0, 5.0,
        ]);

        #[rustfmt::skip]
        let expect = Matrix::new(4, 4, vec![
                6.0, 6.0, 6.0, 6.0,
                0.0, 0.0, 0.0, 0.0,
                2.0, 2.0, 2.0, 2.0,
                0.0, 0.0, 0.0, 0.0,
        ]);

        assert!(test_mul_fun(scalar::matmul_into, &a, &b, &expect));
        assert!(test_mul_fun(scalar::matmul_naive_into, &a, &b, &expect));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<2>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<4>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<8>,
            &a,
            &b,
            &expect
        ));
    }

    #[test]
    fn remainder_5x4()
    {
        #[rustfmt::skip]
        let a = Matrix::new(5, 4, vec![
            1.0, 1.0, 1.0, 1.0,
            0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0,

            0.0, 0.0, 0.0, 0.0,
        ]);

        #[rustfmt::skip]
        let b = Matrix::new(4, 5, vec![
            1.0, 0.0, 0.0, 0.0,  0.0,
            0.0, 0.0, 0.0, 0.0,  0.0,
            0.0, 0.0, 0.0, 0.0,  0.0,
            0.0, 0.0, 0.0, 0.0,  0.0,
        ]);

        let mut c = Matrix::new_zero(5, 5);

        scalar::matmul_into(&a, &b, &mut c);

        #[rustfmt::skip]
        let expect = Matrix::new(5, 5, vec![
                1.0, 0.0, 0.0, 0.0,  0.0,
                0.0, 0.0, 0.0, 0.0,  0.0,
                0.0, 0.0, 0.0, 0.0,  0.0,
                0.0, 0.0, 0.0, 0.0,  0.0,

                0.0, 0.0, 0.0, 0.0,  0.0,
        ]);

        assert!(test_mul_fun(scalar::matmul_into, &a, &b, &expect));
        assert!(test_mul_fun(scalar::matmul_naive_into, &a, &b, &expect));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<2>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<4>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<8>,
            &a,
            &b,
            &expect
        ));
    }

    #[test]
    fn square_2x2()
    {
        #[rustfmt::skip]
        let a = Matrix::new(2, 2, vec![
            1.0, 0.0,
            0.0, 0.0,
        ]);

        #[rustfmt::skip]
        let b = Matrix::new(2, 2, vec![
            1.0, 1.0,
            2.0, 2.0,
        ]);

        let mut c = Matrix::new_zero(2, 2);

        scalar::matmul_into(&a, &b, &mut c);

        #[rustfmt::skip]
        let expect = Matrix::new(2, 2, vec![
                1.0, 1.0,
                0.0, 0.0,
        ]);

        assert!(test_mul_fun(scalar::matmul_into, &a, &b, &expect));
        assert!(test_mul_fun(scalar::matmul_naive_into, &a, &b, &expect));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<2>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<4>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<8>,
            &a,
            &b,
            &expect
        ));
    }

    #[test]
    fn rect_1x3()
    {
        #[rustfmt::skip]
        let a = Matrix::new(1, 5, vec![
            1.0, 0.0, 0.0, 0.0,  0.0,
        ]);

        #[rustfmt::skip]
        let b = Matrix::new(5, 3, vec![
            -1.0, 0.0, 1.0,
            -2.0, 2.0, 2.0,
            -3.0, 4.0, 3.0,
            -4.0, 6.0, 4.0,
            -5.0, 8.0, 5.0,
        ]);

        #[rustfmt::skip]
        let expect = Matrix::new(1, 3, vec![
                -1.0, 0.0, 1.0,
        ]);

        assert!(test_mul_fun(scalar::matmul_into, &a, &b, &expect));
        assert!(test_mul_fun(scalar::matmul_naive_into, &a, &b, &expect));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<2>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<4>,
            &a,
            &b,
            &expect
        ));
        assert!(test_mul_fun(
            scalar::matmul_branching_into::<8>,
            &a,
            &b,
            &expect
        ));
    }

    fn mk_rand_mat(rows: usize, cols: usize) -> Matrix
    {
        use rand_chacha::{rand_core::{RngCore, SeedableRng},
                          ChaCha8Rng};

        let mut rng = ChaCha8Rng::seed_from_u64(42);

        let mut data = vec![0.0; rows * cols];

        data.fill_with(|| rng.next_u32() as f64);

        Matrix { cols, rows, data }
    }

    #[test]
    fn eq()
    {
        fn test_rand(m: usize, n: usize, k: usize)
        {
            let a = mk_rand_mat(m, n);
            let b = mk_rand_mat(n, k);

            let mut c1 = Matrix::new_zero(m, k);
            let mut c2 = Matrix::new_zero(m, k);
            let mut c3 = Matrix::new_zero(m, k);
            let mut c4 = Matrix::new_zero(m, k);

            scalar::matmul_into(&a, &b, &mut c1);
            scalar::matmul_naive_into(&a, &b, &mut c2);
            scalar::matmul_branching_into::<4>(&a, &b, &mut c3);
            scalar::matmul_branching_into::<8>(&a, &b, &mut c4);

            //assert_eq!(c1, c2, "c1 != c2");
            //assert_eq!(c1, c3, "c1 != c3");
            //assert_eq!(c1, c4, "c1 != c4");
            assert_eq!(c2, c3, "c2 != c3");
            assert_eq!(c2, c4, "c2 != c3");
        }

        test_rand(3, 5, 7);
        test_rand(4, 4, 4);
        test_rand(5, 5, 5);
        test_rand(11, 5, 1);
        test_rand(1337, 23, 42);
    }
}
