SIMD impls of distance
================================================================================

- ``dist_naive``
- ``dist_x4``, using *wide*, 256 b, 4 doubles per loop
- ``dist_x4_x2``, using *wide*, 2×256 b, 4×2 doubles per loop
- ``dist_x8``, using *packed_simd*, 2×512 b, 8 doubles per loop

timings
================================================================================

AMD Phenom 2 (SSE4a)
--------------------------------------------------------------------------------

- ``release``::

    dist/naive/100000       time:   [282.78 µs 283.23 µs 283.86 µs]
    dist/f64x4/100000       time:   [246.93 µs 247.21 µs 247.50 µs]
    dist/f64x4x2/100000     time:   [216.51 µs 219.67 µs 224.77 µs]
    dist/f64x8/100000       time:   [216.27 µs 217.41 µs 218.13 µs]

- ``native``::

    dist/naive/100000       time:   [276.29 µs 277.88 µs 280.36 µs]
    dist/f64x4/100000       time:   [230.94 µs 234.78 µs 238.42 µs]
    dist/f64x4x2/100000     time:   [244.93 µs 245.46 µs 246.08 µs]
    dist/f64x8/100000       time:   [231.55 µs 232.66 µs 234.65 µs]

Intel i7-6600U (Skylake, AVX2)
--------------------------------------------------------------------------------

- ``release``::

    dist/naive/100000       time:   [127.52 µs 128.55 µs 129.82 µs]
    dist/f64x4/100000       time:   [56.184 µs 56.876 µs 57.784 µs]
    dist/f64x4x2/100000     time:   [57.303 µs 57.482 µs 57.641 µs]
    dist/f64x8/100000       time:   [56.320 µs 56.775 µs 57.117 µs]

- ``native``::

    dist/naive/100000       time:   [127.64 µs 128.94 µs 130.28 µs]
    dist/f64x4/100000       time:   [50.027 µs 50.412 µs 50.811 µs]
    dist/f64x4x2/100000     time:   [45.436 µs 45.793 µs 46.265 µs]
    dist/f64x8/100000       time:   [45.819 µs 46.221 µs 46.683 µs]

profiling / code
================================================================================

- SSE2/AMD64::
    - ``naive``
      ::

         3.50 │ 60:   movsd  (%rdi,%rax,8),%xmm2
         3.41 │       subsd  (%rdx,%rax,8),%xmm2
         3.18 │       movsd  0x8(%rdi,%rax,8),%xmm0
         3.41 │       mulsd  %xmm2,%xmm2
         3.45 │       movsd  (%r8,%rax,8),%xmm3
         3.74 │       movsd  0x8(%r8,%rax,8),%xmm4
         3.55 │       mulsd  %xmm3,%xmm2
         3.45 │       mulsd  %xmm3,%xmm2
         3.95 │       subsd  0x8(%rdx,%rax,8),%xmm0
         5.22 │       addsd  %xmm1,%xmm2
         5.76 │       add    $0x2,%rax
         6.08 │       mulsd  %xmm0,%xmm0
         7.48 │       mulsd  %xmm4,%xmm0
         7.29 │       mulsd  %xmm4,%xmm0
         7.55 │       addsd  %xmm2,%xmm0
        10.06 │       movapd %xmm0,%xmm1
         9.78 │       cmp    %rax,%rcx
         9.14 │     ↑ jne    60

      … just the one measly ``movapd``, otherwise scalar.

    - ``f64x4``
      ::

         7.04 │ 50:   movupd   (%rdi,%rcx,8),%xmm2
         6.81 │       movupd   0x10(%rdi,%rcx,8),%xmm3
         2.77 │       movupd   (%rdx,%rcx,8),%xmm4
         2.78 │       subpd    %xmm4,%xmm2
         2.93 │       movupd   0x10(%rdx,%rcx,8),%xmm4
         5.12 │       subpd    %xmm4,%xmm3
         5.85 │       movupd   (%r8,%rcx,8),%xmm4
         5.78 │       movupd   0x10(%r8,%rcx,8),%xmm5
         5.62 │       mulpd    %xmm2,%xmm2
        13.34 │       mulpd    %xmm3,%xmm3
        13.31 │       mulpd    %xmm4,%xmm2
        10.31 │       mulpd    %xmm5,%xmm3
         2.15 │       mulpd    %xmm4,%xmm2
         2.15 │       addpd    %xmm2,%xmm1
         2.23 │       mulpd    %xmm5,%xmm3
         1.57 │       addpd    %xmm3,%xmm0
         1.74 │       add      $0x4,%rcx
         2.55 │       cmp      %rcx,%rax
         5.93 │     ↑ jne      50

      … much better.

- SSE4a

    - ``naive``
      ::

         9.31 │30:   movsd  (%rdi,%rax,8),%xmm1
        10.29 │      subsd  (%rdx,%rax,8),%xmm1
        14.85 │      movsd  (%r8,%rax,8),%xmm2
        11.13 │      inc    %rax
        10.38 │      mulsd  %xmm1,%xmm1
         9.40 │      mulsd  %xmm2,%xmm1
         8.28 │      mulsd  %xmm2,%xmm1
         7.78 │      addsd  %xmm1,%xmm0
         7.87 │      cmp    %rax,%rsi
        10.72 │    ↑ jne    30

      … all scalar again‽

    - ``f64x4``
      ::

         6.67 │ 50:   movupd   (%rdi,%rcx,8),%xmm2
         6.81 │       movupd   0x10(%rdi,%rcx,8),%xmm3
         3.17 │       movupd   (%rdx,%rcx,8),%xmm4
         2.66 │       subpd    %xmm4,%xmm2
         2.59 │       movupd   0x10(%rdx,%rcx,8),%xmm4
         6.23 │       subpd    %xmm4,%xmm3
         6.92 │       movupd   (%r8,%rcx,8),%xmm4
         6.84 │       movupd   0x10(%r8,%rcx,8),%xmm5
         4.89 │       mulpd    %xmm2,%xmm2
        12.61 │       mulpd    %xmm3,%xmm3
        12.46 │       mulpd    %xmm4,%xmm2
        10.53 │       mulpd    %xmm5,%xmm3
         2.10 │       mulpd    %xmm4,%xmm2
         2.14 │       addpd    %xmm2,%xmm1
         2.16 │       mulpd    %xmm5,%xmm3
         1.63 │       addpd    %xmm3,%xmm0
         1.52 │       add      $0x4,%rcx
         2.29 │       cmp      %rcx,%rax
         5.78 │     ↑ jne      50

- AVX2

    - ``f64x4``
      ::

         0.03 │ a0:   vmovupd      (%rcx,%rbx,8),%ymm1
         4.46 │       vmovupd      0x20(%rcx,%rbx,8),%ymm2
         0.08 │       vmovupd      0x40(%rcx,%rbx,8),%ymm3
         3.55 │       vmovupd      0x60(%rcx,%rbx,8),%ymm4
         3.93 │       vsubpd       (%r11,%rbx,8),%ymm1,%ymm1
         0.03 │       vmulpd       %ymm1,%ymm1,%ymm1
         0.35 │       vmovupd      (%r10,%rbx,8),%ymm5
         1.17 │       vmovupd      0x20(%r10,%rbx,8),%ymm6
         3.91 │       vmovupd      0x40(%r10,%rbx,8),%ymm7
         5.88 │       vmovupd      0x60(%r10,%rbx,8),%ymm8
         0.29 │       vmulpd       %ymm1,%ymm5,%ymm1
         0.15 │       vmulpd       %ymm1,%ymm5,%ymm1
         3.82 │       vaddpd       %ymm1,%ymm0,%ymm0
         3.28 │       vsubpd       0x20(%r11,%rbx,8),%ymm2,%ymm1
         1.80 │       vmulpd       %ymm1,%ymm1,%ymm1
         5.32 │       vmulpd       %ymm1,%ymm6,%ymm1
         4.25 │       vmulpd       %ymm1,%ymm6,%ymm1
        13.02 │       vaddpd       %ymm1,%ymm0,%ymm0
         0.09 │       vsubpd       0x40(%r11,%rbx,8),%ymm3,%ymm1
         0.30 │       vmulpd       %ymm1,%ymm1,%ymm1
         3.94 │       vmulpd       %ymm1,%ymm7,%ymm1
         0.18 │       vmulpd       %ymm1,%ymm7,%ymm1
        11.55 │       vaddpd       %ymm1,%ymm0,%ymm0
         3.08 │       vsubpd       0x60(%r11,%rbx,8),%ymm4,%ymm1
         1.90 │       vmulpd       %ymm1,%ymm1,%ymm1
         5.50 │       vmulpd       %ymm1,%ymm8,%ymm1
         4.23 │       vmulpd       %ymm1,%ymm8,%ymm1
        13.81 │       vaddpd       %ymm1,%ymm0,%ymm0

      :)

dynamic multiversioning
================================================================================

Picking the right implementation:

- Phenom II (max SSE2):
  ``dist::dist_naive_dynamic::dist_naive_dynamic_default_version``

- Skylake (max AVX2):
  ``dist::dist_naive_dynamic::dist_naive_dynamic_avx_avx2_fma_sse_sse2_sse3_sse41_ssse3_version``

     - The naive version is still disappointingly unoptimized, which reminds
       of Linus_:
       ::

        13.63 │ 60:   vmovsd  (%rdi,%rax,8),%xmm1
         0.10 │       vmovsd  0x8(%rdi,%rax,8),%xmm2
        17.08 │       vsubsd  (%rdx,%rax,8),%xmm1,%xmm1
         0.67 │       vmulsd  %xmm1,%xmm1,%xmm1
         7.36 │       vmovsd  (%r8,%rax,8),%xmm3
         8.18 │       vmovsd  0x8(%r8,%rax,8),%xmm4
         0.95 │       vmulsd  %xmm1,%xmm3,%xmm1
         1.04 │       vmulsd  %xmm1,%xmm3,%xmm1
        17.31 │       vaddsd  %xmm1,%xmm0,%xmm0
         0.43 │       vsubsd  0x8(%rdx,%rax,8),%xmm2,%xmm1
         0.10 │       add     $0x2,%rax
         8.09 │       vmulsd  %xmm1,%xmm1,%xmm1
         0.04 │       vmulsd  %xmm1,%xmm4,%xmm1
         0.15 │       vmulsd  %xmm1,%xmm4,%xmm1
        24.81 │       vaddsd  %xmm1,%xmm0,%xmm0

.. _Linus: https://www.realworldtech.com/forum/?threadid=209249&curpostid=209596

parallelism
================================================================================

Adding Rayon to the mix naturally slows down operations for small *n* but leads
to tremendous improvements for large ones. On *Phenom II* the breakeven point
seems to be around *10⁴* for the naive (scalar ops heavy) implementation, but
somewhat higher for the hand vectored ones::

    dist/naive/10000        time:   [20.215 µs 20.216 µs 20.218 µs]
    dist/naive+dyn/10000    time:   [20.231 µs 20.476 µs 20.675 µs]
    dist/naive+par/10000    time:   [19.490 µs 20.047 µs 20.847 µs]
    dist/f64x4/10000        time:   [10.205 µs 10.272 µs 10.342 µs]
    dist/f64x4+dyn/10000    time:   [10.270 µs 10.281 µs 10.293 µs]
    dist/f64x4+par/10000    time:   [15.744 µs 16.117 µs 16.593 µs]

    dist/f64x4/100000       time:   [210.87 µs 212.50 µs 215.46 µs]
    dist/f64x4+dyn/100000   time:   [238.17 µs 238.38 µs 238.73 µs]
    dist/f64x4+par/100000   time:   [94.482 µs 98.810 µs 103.27 µs]

Skylake OTOH is absolutely killing it in terms of single threaded performance
so at *10⁴*, all sequential impls are still ahead: ::

    dist/naive/10000        time:   [12.558 µs 12.594 µs 12.629 µs]
    dist/naive+dyn/10000    time:   [12.502 µs 12.647 µs 12.839 µs]
    dist/naive+par/10000    time:   [18.666 µs 18.805 µs 19.003 µs]
    dist/f64x4/10000        time:   [4.8761 µs 4.8974 µs 4.9232 µs]
    dist/f64x4+dyn/10000    time:   [4.5761 µs 4.6101 µs 4.6468 µs]
    dist/f64x4+par/10000    time:   [16.601 µs 16.672 µs 16.746 µs]
    dist/f64x8/10000        time:   [4.8329 µs 4.8473 µs 4.8644 µs]
    dist/f64x8+dyn/10000    time:   [3.4098 µs 3.4312 µs 3.4533 µs]
    dist/f64x8+par/10000    time:   [16.847 µs 16.967 µs 17.091 µs]

whereas at *10⁵*, parallelism beats all implementations except for the
multiversioned 512 bit one: ::

    dist/naive/100000       time:   [124.85 µs 125.42 µs 126.04 µs]
    dist/naive+dyn/100000   time:   [125.55 µs 126.43 µs 127.37 µs]
    dist/naive+par/100000   time:   [65.754 µs 67.452 µs 69.434 µs]
    dist/f64x4/100000       time:   [53.560 µs 53.933 µs 54.381 µs]
    dist/f64x4+dyn/100000   time:   [51.282 µs 51.596 µs 51.903 µs]
    dist/f64x4+par/100000   time:   [45.619 µs 46.078 µs 46.582 µs]
    dist/f64x8/100000       time:   [53.085 µs 53.374 µs 53.735 µs]
    dist/f64x8+dyn/100000   time:   [40.507 µs 40.734 µs 40.966 µs]
    dist/f64x8+par/100000   time:   [44.323 µs 45.009 µs 46.027 µs]

At even larger inputs the vectored sequential implementations are about 1.2
(Skylake) and 1.9 (Phenom II) times slower than the parallel equivalent. The
benefit of combining parallelism and multiversioning is consistent but small:

    - Skylake: ::

        dist/f64x8/10000000         time:   [18.898 ms 18.942 ms 18.987 ms]
        dist/f64x8+dyn/10000000     time:   [18.330 ms 18.387 ms 18.477 ms]
        dist/f64x8+par/10000000     time:   [15.514 ms 15.585 ms 15.670 ms]
        dist/f64x8+par+dyn/10000000 time:   [15.500 ms 15.568 ms 15.675 ms]

    - Phenom 2: ::

        dist/f64x4/10000000         time:   [48.954 ms 49.086 ms 49.238 ms]
        dist/f64x4+dyn/10000000     time:   [49.018 ms 49.134 ms 49.242 ms]
        dist/f64x4+par/10000000     time:   [24.485 ms 25.161 ms 26.042 ms]
        dist/f64x4+par+dyn/10000000 time:   [24.341 ms 24.855 ms 25.389 ms]

        dist/f64x8/10000000         time:   [47.300 ms 47.369 ms 47.433 ms]
        dist/f64x8+dyn/10000000     time:   [47.217 ms 47.381 ms 47.519 ms]
        dist/f64x8+par/10000000     time:   [24.173 ms 24.738 ms 25.287 ms]
        dist/f64x8+par+dyn/10000000 time:   [24.679 ms 25.512 ms 26.469 ms]

Which is probably due to the memory bandwidth – which is shared between all the
cores – becoming the main bottleneck.

