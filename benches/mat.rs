use std::hint::black_box;

use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion};

use dist::tiled::{scalar::{mat_mul_vec_into, mat_mul_vec_into_naive,
                           matmul_branching_into, matmul_into,
                           matmul_naive_into, vec_mul_mat_into,
                           vec_mul_mat_into_naive},
                  Matrix};

const MATVEC_INPUTS: &[(usize, usize)] = &[
    //(4, 1),
    //(42, 1337),
    (100_000, 1_000),
];

const MATMAT_INPUTS: &[(usize, usize, usize)] = &[
    (51, 23, 71),
    (501, 203, 701),
    //(1000, 1000, 1000),
    //(5001, 2003, 7001),
    //(10_000, 100, 15_000),
];

fn mk_inputs_matvec(rows: usize, cols: usize) -> (Matrix, Vec<f64>, Vec<f64>)
{
    use rand_chacha::{rand_core::{RngCore, SeedableRng},
                      ChaCha8Rng};

    let mut rng = ChaCha8Rng::seed_from_u64(42);

    let mut v_cols = vec![0.0; cols];
    let mut v_rows = vec![0.0; rows];
    let mut m = vec![0.0; rows * cols];

    v_cols.fill_with(|| rng.next_u32() as f64);
    v_rows.fill_with(|| rng.next_u32() as f64);
    m.fill_with(|| rng.next_u32() as f64);

    let m = Matrix::new(rows, cols, m);

    (m, v_rows, v_cols)
}

fn mk_inputs_matmat(m: usize, n: usize, k: usize) -> (Matrix, Matrix, Matrix)
{
    use rand_chacha::{rand_core::{RngCore, SeedableRng},
                      ChaCha8Rng};

    let mut rng = ChaCha8Rng::seed_from_u64(42);

    let mut a = vec![0.0; m * n];
    let mut b = vec![0.0; n * k];
    let mut c = vec![0.0; m * k];

    a.fill_with(|| rng.next_u32() as f64);
    b.fill_with(|| rng.next_u32() as f64);
    c.fill_with(|| rng.next_u32() as f64);

    let a = Matrix::new(m, n, a);
    let b = Matrix::new(n, k, b);
    let c = Matrix::new(m, k, c);

    (a, b, c)
}

fn mat_vec(c: &mut Criterion)
{
    let mut g = c.benchmark_group("mat_vec_mul");

    MATVEC_INPUTS.iter().for_each(|&(rows, cols)| {
        let (m, mut out, v) = mk_inputs_matvec(rows, cols);

        g.bench_function(
            BenchmarkId::new(
                "naive",
                &format!("{}×{}·{}={}", rows, cols, cols, rows),
            ),
            |b| {
                b.iter(|| {
                    black_box(mat_mul_vec_into_naive(
                        &m,
                        v.as_slice(),
                        out.as_mut_slice(),
                    ));
                    out.fill(0.0);
                })
            },
        );

        g.bench_function(
            BenchmarkId::new(
                "tiled",
                &format!("{}×{}·{}={}", rows, cols, cols, rows),
            ),
            |b| {
                b.iter(|| {
                    black_box(mat_mul_vec_into(
                        &m,
                        v.as_slice(),
                        out.as_mut_slice(),
                    ));
                    out.fill(0.0);
                })
            },
        );
    });

    g.finish();
}

fn vec_mat(c: &mut Criterion)
{
    let mut g = c.benchmark_group("vec_mat_mul");

    MATVEC_INPUTS.iter().for_each(|&(rows, cols)| {
        let (m, v, mut out) = mk_inputs_matvec(rows, cols);

        g.bench_function(
            BenchmarkId::new(
                "naive",
                &format!("{}·{}×{}={}", rows, rows, cols, cols),
            ),
            |b| -> () {
                b.iter(|| {
                    black_box(vec_mul_mat_into_naive(
                        v.as_slice(),
                        &m,
                        out.as_mut_slice(),
                    ));
                    out.fill(0.0);
                })
            },
        );

        g.bench_function(
            BenchmarkId::new(
                "tiled",
                &format!("{}·{}×{}={}", rows, rows, cols, cols),
            ),
            |b| {
                b.iter(|| {
                    black_box(vec_mul_mat_into(
                        v.as_slice(),
                        &m,
                        out.as_mut_slice(),
                    ));
                    out.fill(0.0);
                })
            },
        );
    });

    g.finish();
}

fn mat_mat(c: &mut Criterion)
{
    let mut g = c.benchmark_group("mat_mat_mul");

    MATMAT_INPUTS.iter().for_each(|&(m, n, k)| {
        let (ma, mb, mut mc) = mk_inputs_matmat(m, n, k);

        g.bench_function(
            BenchmarkId::new(
                "tiled",
                &format!("{}×{}·{}×{}={}×{}", m, n, n, k, m, k),
            ),
            |b| {
                b.iter(|| {
                    black_box(matmul_into(&ma, &mb, &mut mc));
                    mc.data_mut().fill(0.0);
                })
            },
        );

        g.bench_function(
            BenchmarkId::new(
                "tiled+branch+2",
                &format!("{}×{}·{}×{}={}×{}", m, n, n, k, m, k),
            ),
            |b| {
                b.iter(|| {
                    black_box(matmul_branching_into::<2>(&ma, &mb, &mut mc));
                    mc.data_mut().fill(0.0);
                })
            },
        );

        g.bench_function(
            BenchmarkId::new(
                "tiled+branch+4",
                &format!("{}×{}·{}×{}={}×{}", m, n, n, k, m, k),
            ),
            |b| {
                b.iter(|| {
                    black_box(matmul_branching_into::<4>(&ma, &mb, &mut mc));
                    mc.data_mut().fill(0.0);
                })
            },
        );

        g.bench_function(
            BenchmarkId::new(
                "tiled+branch+8",
                &format!("{}×{}·{}×{}={}×{}", m, n, n, k, m, k),
            ),
            |b| {
                b.iter(|| {
                    black_box(matmul_branching_into::<8>(&ma, &mb, &mut mc));
                    mc.data_mut().fill(0.0);
                })
            },
        );

        g.bench_function(
            BenchmarkId::new(
                "naive",
                &format!("{}×{}·{}×{}={}×{}", m, n, n, k, m, k),
            ),
            |b| {
                b.iter(|| {
                    black_box(matmul_naive_into(&ma, &mb, &mut mc));
                    mc.data_mut().fill(0.0);
                })
            },
        );
    });

    g.finish();
}

criterion_group! {
    name = benches;

    config = Criterion::default()
        .warm_up_time(std::time::Duration::from_secs(1))
        .measurement_time(std::time::Duration::from_secs(10))
        .sample_size(20);

    targets = mat_vec, vec_mat, mat_mat
}

criterion_main!(benches);
