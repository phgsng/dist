use std::hint::black_box;

use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion};

use dist::{dist_naive, dist_naive_dyn, dist_naive_par, dist_naive_par_dyn,
           dist_progressive, dist_psmd_x4, dist_psmd_x4_dyn, dist_psmd_x4_par,
           dist_psmd_x4_par_dyn, dist_wide_x4, dist_wide_x4_dyn,
           dist_wide_x4_par, dist_wide_x4_par_dyn, dist_wide_x4_x2, dist_x8,
           dist_x8_dyn, dist_x8_par, dist_x8_par_dyn};

//const INPUT_RANGE: std::ops::Range<usize> = 2..24;
const INPUT_RANGE: std::ops::Range<usize> = 19..20;

fn mk_inputs(n: usize) -> (Vec<f64>, Vec<f64>, Vec<f64>)
{
    use rand_chacha::{rand_core::{RngCore, SeedableRng},
                      ChaCha8Rng};

    let mut rng = ChaCha8Rng::seed_from_u64(42);

    let mut x = vec![0.0; n];
    let mut y = vec![0.0; n];
    let mut d = vec![0.0; n];

    x.fill_with(|| rng.next_u32() as f64);
    y.fill_with(|| rng.next_u32() as f64);
    d.fill_with(|| rng.next_u32() as f64);

    (x, y, d)
}

/**
 * For sake of comparison a no-op implementation that reads all the input memory
 * consecutively to determine the memory bandwith.
 *
 * Remainders are too insignificant for us to care about them here.
 */
#[inline(never)]
fn memory(x0: &[f64], x1: &[f64], d: &[f64]) -> f64
{
    use rayon::prelude::*;

    let chunk_size = (x0.len() / rayon::max_num_threads()).max(8);

    let x = x0
        .par_chunks_exact(chunk_size)
        .zip(x1.par_chunks_exact(chunk_size))
        .zip(d.par_chunks_exact(chunk_size))
        .map(|((x0, x1), d)| {
            x0.iter()
                .zip(x1.iter())
                .zip(d.iter())
                .map(|((x0, x1), d)| x0 + x1 + d)
                .last()
                .unwrap()
        })
        .sum::<f64>();
    x
}

fn dist(c: &mut Criterion)
{
    let mut g = c.benchmark_group("dist");
    INPUT_RANGE.for_each(|pow| {
        let n = 1usize << pow;
        let inputs = mk_inputs(n);
        let inputs: (&[f64], &[f64], &[f64]) =
            (inputs.0.as_slice(), inputs.1.as_slice(), inputs.2.as_slice());

        let mut bench =
            |id: &str, n: usize, f: fn(&[f64], &[f64], &[f64]) -> f64| {
                let id = BenchmarkId::new(id, &format!("2^{}|{}", pow, n));
                g.bench_with_input(
                    id,
                    &inputs,
                    |b, is: &(&[f64], &[f64], &[f64])| {
                        b.iter(|| black_box(f(is.0, is.1, is.2)))
                    },
                );
            };

        bench("memory", n, memory);
        bench("progressive", n, dist_progressive);
        bench("naive", n, dist_naive);
        bench("naive+dyn", n, dist_naive_dyn);
        bench("naive+par", n, dist_naive_par);
        bench("naive+par+dyn", n, dist_naive_par_dyn);
        bench("x4-wide", n, dist_wide_x4);
        bench("x4-wide+dyn", n, dist_wide_x4_dyn);
        bench("x4-wide+par", n, dist_wide_x4_par);
        bench("x4-wide+par+dyn", n, dist_wide_x4_par_dyn);
        bench("x4-psmd", n, dist_psmd_x4);
        bench("x4-psmd+dyn", n, dist_psmd_x4_dyn);
        bench("x4-psmd+par", n, dist_psmd_x4_par);
        bench("x4-psmd+par+dyn", n, dist_psmd_x4_par_dyn);
        bench("x4-wide+x2", n, dist_wide_x4_x2);
        bench("f64x8", n, dist_x8);
        bench("f64x8+dyn", n, dist_x8_dyn);
        bench("f64x8+par", n, dist_x8_par);
        bench("f64x8+par+dyn", n, dist_x8_par_dyn);
    });
    g.finish();
}

criterion_group! {
    name = benches;

    config = Criterion::default()
        /*
         * On early Skylake, there’s a warmup penalty of 56,000 cycles
         * for %YMM and %ZMM operations, so 1s should be enough here:
         * https://www.agner.org/optimize/microarchitecture.pdf
         */
        .warm_up_time(std::time::Duration::from_secs(1))
        .measurement_time(std::time::Duration::from_secs(10))
        .sample_size(20);

    targets = dist
}

criterion_main!(benches);
