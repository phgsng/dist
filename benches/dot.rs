use std::hint::black_box;

use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion};

use dist::neu::{dot_generic, dot_simd_x4, dot_simd_x8};

const DOT_INPUTS: &[usize] = &[2, 4, 10, 20, 25];

fn mk_inputs_dot(n: usize) -> (Vec<f64>, Vec<f64>)
{
    use rand_chacha::{rand_core::{RngCore, SeedableRng},
                      ChaCha8Rng};

    let mut rng = ChaCha8Rng::seed_from_u64(42);

    let mut x = vec![0.0; n];
    let mut y = vec![0.0; n];

    x.fill_with(|| rng.next_u32() as f64);
    y.fill_with(|| rng.next_u32() as f64);

    (x, y)
}

fn neumaier(c: &mut Criterion)
{
    let mut g = c.benchmark_group("neumaier");

    DOT_INPUTS.iter().for_each(|&pow2| {
        let n = 1usize << pow2;
        let (x, y) = mk_inputs_dot(n);

        g.bench_function(
            BenchmarkId::new("generic-2", &format!("2^{}|{}", pow2, n)),
            |b| b.iter(|| black_box(dot_generic::<2>(&x, &y))),
        );

        g.bench_function(
            BenchmarkId::new("generic-4", &format!("2^{}|{}", pow2, n)),
            |b| b.iter(|| black_box(dot_generic::<4>(&x, &y))),
        );

        g.bench_function(
            BenchmarkId::new("generic-8", &format!("2^{}|{}", pow2, n)),
            |b| b.iter(|| black_box(dot_generic::<8>(&x, &y))),
        );

        g.bench_function(
            BenchmarkId::new("simd-4", &format!("2^{}|{}", pow2, n)),
            |b| b.iter(|| black_box(dot_simd_x4(&x, &y))),
        );

        g.bench_function(
            BenchmarkId::new("simd-8", &format!("2^{}|{}", pow2, n)),
            |b| b.iter(|| black_box(dot_simd_x8(&x, &y))),
        );
    });

    g.finish();
}

criterion_group! {
    name = benches;

    config = Criterion::default()
        .warm_up_time(std::time::Duration::from_secs(1))
        .measurement_time(std::time::Duration::from_secs(20))
        .sample_size(20);

    targets = neumaier,
}

criterion_main!(benches);
